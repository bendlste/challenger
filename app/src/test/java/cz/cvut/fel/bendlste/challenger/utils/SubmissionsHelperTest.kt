package cz.cvut.fel.bendlste.challenger.utils

import cz.cvut.fel.bendlste.challenger.model.Submission
import org.junit.Test

import org.junit.Assert.*

class SubmissionsHelperTest {

    @Test
    fun calculateTotalValueForUser_singleUser() {
        val challengeId = "0"
        val userId = "1"
        val timestamp = 0L

        val s1 = Submission(challengeId, userId, 10, timestamp)
        val s2 = Submission(challengeId, userId, 20, timestamp)
        val s3 = Submission(challengeId, userId, 30, timestamp)

        val submissions = listOf(s1, s2, s3)

        val total = SubmissionsHelper.calculateTotalValueForUser(userId, submissions)

        assertEquals("Total count does not match.", 60, total)
    }

    @Test
    fun calculateTotalValueForUser_multipleUsers() {
        val challengeId = "0"
        val userId = "1"
        val timestamp = 0L

        val s1 = Submission(challengeId, userId, 10, timestamp)
        val s2 = Submission(challengeId, userId, 20, timestamp)
        val s3 = Submission(challengeId, "2", 30, timestamp)

        val submissions = listOf(s1, s2, s3)

        val total = SubmissionsHelper.calculateTotalValueForUser(userId, submissions)

        assertEquals("Total count does not match.",30, total)
    }
}