package cz.cvut.fel.bendlste.challenger.model

import org.junit.Test

import org.junit.Assert.*
import java.util.Calendar

class ChallengeTest {

    @Test
    fun isActive_true() {
        // Set up calendar and time
        val time = getCustomTimestampHoursDifference(1)

        val challenge = createMockChallenge(to = time)

        assertTrue("Challenge should be active", challenge.isActive())
    }

    @Test
    fun isActive_false() {
        // Set up calendar and time
        val time = getCustomTimestampHoursDifference(-1)

        val challenge = createMockChallenge(to = time)

        assertFalse("Challenge shouldn't be active", challenge.isActive())
    }

    @Test
    fun isUpcoming_true() {
        val from = getCustomTimestampHoursDifference(1)

        val challenge = createMockChallenge(from = from)

        assertTrue("Challenge should be upcoming", challenge.isUpcoming())
    }

    @Test
    fun isUpcoming_false() {
        val from = getCustomTimestampHoursDifference(-1)

        val challenge = createMockChallenge(from = from)

        assertFalse("Challenge should be upcoming", challenge.isUpcoming())
    }

    private fun createMockChallenge(
        from: Long = 0,
        to: Long = 0,
        target: Int = 10,
        milestones: List<Int> = listOf(3, 6)
    ): Challenge {
        return Challenge(
            uuid = "1",
            name = "a",
            description = "a",
            from = from,
            to = to,
            targetValue = target,
            milestones = milestones,
            type = ChallengeType.FIRST_TO_THE_FINISH
        )
    }

    private fun getCustomTimestampHoursDifference(hours: Int): Long {
        return Calendar.getInstance().apply {
            add(Calendar.HOUR, hours)
        }.timeInMillis
    }
}