package cz.cvut.fel.bendlste.challenger.ui.explore

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import cz.cvut.fel.bendlste.challenger.databinding.ActivityInviteFriendsBinding
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.ui.friends.UserProfileActivity
import cz.cvut.fel.bendlste.challenger.viewModel.explore.InviteFriendsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InviteFriendsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityInviteFriendsBinding
    private lateinit var challengeId: String
    private lateinit var inviteFriendsViewModel: InviteFriendsViewModel
    private lateinit var adapter: InviteFriendsAdapter
    private var challenge: Challenge? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityInviteFriendsBinding.inflate(layoutInflater)
        inviteFriendsViewModel = ViewModelProvider(this).get(InviteFriendsViewModel::class.java)
        setContentView(binding.root)
        supportActionBar?.hide()

        getChallengeIdFromIntent()
        fetchData()
        setUpAdapter()
    }

    /**
     * Sets up [InviteFriendsAdapter].
     */
    private fun setUpAdapter() {
        adapter = InviteFriendsAdapter(
            { userId -> adapterOnClick(userId) },
            { userId -> inviteOnClick(userId) },
            this
        )
        val inviteFriendsRecyclerView = binding.ifRecycler
        inviteFriendsRecyclerView.adapter = adapter

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        inviteFriendsRecyclerView.layoutManager = layoutManager

        val dividerItemDecoration = DividerItemDecoration(
            inviteFriendsRecyclerView.context,
            layoutManager.orientation
        )
        inviteFriendsRecyclerView.addItemDecoration(dividerItemDecoration)
    }

    /**
     * Retrieves [challengeId] from intent.
     */
    private fun getChallengeIdFromIntent() {
        val challengeIdIntent = intent.getSerializableExtra("CHALLENGE_ID")
        challengeId = if (challengeIdIntent is String) {
            challengeIdIntent
        } else ""
    }

    /**
     * Retrieves data from Firebase Database and passes them to adapter.
     */
    private fun fetchData() {
        // Get challenge
        inviteFriendsViewModel.getChallenge(challengeId).observe(this, {
            if (it != null) {
                challenge = it
                binding.inviteFriendsTxtSubtitle.text = it.name
            }
        })

        inviteFriendsViewModel.friendIds.observe(this, { ids ->
            inviteFriendsViewModel.getFriendsFromIds(ids).observe(this, { friends ->
                if (friends != null) {
                    adapter.submitList(friends.filter { user -> !challenge?.participantsIds!!.contains(user.uuid) })
                }
            })
        })
    }

    /**
     * Launches [UserProfileActivity].
     */
    private fun adapterOnClick(userId: String) {
        val intent = Intent(this, UserProfileActivity::class.java)
        intent.putExtra("USER_ID", userId)
        startActivity(intent)
    }

    /**
     * Sends a challenge invite to selected friend.
     */
    private fun inviteOnClick(userId: String): Boolean {
        challenge?.let {
            inviteFriendsViewModel.addChallengeInvite(it, userId)
            return true
        }
        return false
    }
}