package cz.cvut.fel.bendlste.challenger.model

import com.google.firebase.database.Exclude
import java.io.Serializable

/**
 * Represents user.
 */
data class User(
    var uuid: String,
    var name: String?,
    var email: String,
    var age: Int? = null
) : Serializable {

    @get:Exclude
    var isAuthenticated = false

    @get:Exclude
    var isNew = false

    @get:Exclude
    var isCreated = false

    @get:Exclude
    var challenges: List<Challenge>? = null

    companion object {
        /**
         * Converts HashMap with proper values to a [User] object.
         */
        @Exclude
        fun convertFromHashMap(hashMap: HashMap<String, Any?>): User {
            val name = hashMap["fullName"] as String
            val uuid = hashMap["uuid"] as String
            val age = (hashMap["age"] as Long).toInt()
            val email = hashMap["email"] as String

            return User(uuid, name, email, age)
        }
    }
}