package cz.cvut.fel.bendlste.challenger.ui.explore

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.databinding.ActivityChallengeDetailBinding
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.Submission
import cz.cvut.fel.bendlste.challenger.model.User
import cz.cvut.fel.bendlste.challenger.ui.friends.UserProfileActivity
import cz.cvut.fel.bendlste.challenger.utils.GlideApp
import cz.cvut.fel.bendlste.challenger.utils.SubmissionsHelper
import cz.cvut.fel.bendlste.challenger.viewModel.explore.ChallengeDetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.Date

@AndroidEntryPoint
class ChallengeDetailActivity : FragmentActivity() {

    private lateinit var challengeId: String
    private lateinit var challengeDetailViewModel: ChallengeDetailViewModel
    private lateinit var challenge: LiveData<Challenge?>
    private lateinit var binding: ActivityChallengeDetailBinding
    private var userSubmissions: HashMap<User, List<Submission>> = HashMap()
    private lateinit var adapter: LeaderboardAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityChallengeDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        challengeDetailViewModel = ViewModelProvider(this).get(ChallengeDetailViewModel::class.java)

        challengeId = getChallengeIdFromIntent()

        challenge = challengeDetailViewModel.getChallenge(challengeId)

        setUpUI()
        setOnClickListeners()
        fetchData()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    /**
     * Retrieves challenge id from intent.
     */
    private fun getChallengeIdFromIntent(): String {
        val challengeId = intent.getSerializableExtra("CHALLENGE_ID")
        if (challengeId is String) {
            return challengeId
        }
        return ""
    }

    /**
     * Sets up UI based on the content of [challenge].
     */
    private fun setUpUI() {
        challenge.observe(this, { challenge ->
            if (challenge != null) {
                binding.challengeDetailTxtTitle.text = challenge.name
                binding.challengeDetailName.text = challenge.name
                binding.challengeDetailDescription.text = challenge.description
                binding.challengeDetailBet.text = challenge.bet

                val from = Date(challenge.from)
                val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
                val fromString = sdf.format(from)
                binding.challengeDetailFrom.text = fromString

                val to = Date(challenge.to)
                val toString = sdf.format(to)
                binding.challengeDetailTo.text = toString

                binding.challengeDetailTarget.text = challenge.targetValue.toString()

                binding.challengeDetailType.text = challenge.type.name

                val currentUserId = Firebase.auth.currentUser?.uid ?: ""

                if (challenge.participantsIds.contains(currentUserId)) {
                    binding.btnJoinChallenge.visibility = View.GONE
                    binding.btnInviteFriend.visibility = View.VISIBLE
                    binding.fabAddSubmission.visibility = View.VISIBLE
                } else {
                    binding.fabAddSubmission.visibility = View.GONE
                    binding.btnInviteFriend.visibility = View.GONE
                    binding.btnJoinChallenge.visibility = View.VISIBLE
                }

                if (challenge.isExpired()) {
                    binding.btnJoinChallenge.visibility = View.GONE
                    binding.btnInviteFriend.visibility = View.GONE
                    binding.fabAddSubmission.visibility = View.GONE
                }

                if (challenge.isUpcoming()) {
                    binding.fabAddSubmission.visibility = View.GONE
                }
            }
        })

        val storageRef = Firebase.storage.reference.child("challenges").child(challengeId)

        storageRef.metadata.addOnSuccessListener {
            GlideApp
                .with(this)
                .load(storageRef)
                .into(binding.challengeDetailImage)
        }.addOnFailureListener {
            binding.challengeDetailImage.setImageResource(R.drawable.default_challenge)
        }
    }

    /**
     * Sets on click listener for buttons.
     */
    private fun setOnClickListeners() {
        binding.btnJoinChallenge.setOnClickListener {
            challengeDetailViewModel.joinChallenge(challengeId)
        }
        binding.fabAddSubmission.setOnClickListener {
            launchAddSubmissionActivity()
        }

        binding.btnInviteFriend.setOnClickListener {
            launchInviteFriendsActivity()
        }
    }

    /**
     * Retrieves data from Firebase database and passes them to adapter.
     */
    private fun fetchData() {
        challengeDetailViewModel.getAllSubmissionsForChallenge(challengeId).observe(this, { submissions ->
            val participantIdsSet = mutableSetOf<String>()
            participantIdsSet.addAll(submissions.map { i -> i.userId })

            challengeDetailViewModel.getParticipants(participantIdsSet.toList()).observe(this, { users ->
                for (participant in users) {
                    userSubmissions[participant] = submissions.filter { s -> s.userId == participant.uuid }
                }
                setUpLeaderboardAdapter()
                val sortedUsers = users.sortedBy {user -> userSubmissions[user]?.let { SubmissionsHelper.calculateTotalValueForUser(user.uuid, it) } }
                adapter.submitList(sortedUsers.asReversed())
            })
        })
    }

    /**
     * Sets up [LeaderboardAdapter].
     */
    private fun setUpLeaderboardAdapter() {
        adapter = LeaderboardAdapter(
            { string -> adapterOnClick(string) },
            this,
            userSubmissions
        )

        val leaderboardRecyclerView = binding.leaderboardsRecyclerView
        leaderboardRecyclerView.adapter = adapter

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        leaderboardRecyclerView.layoutManager = layoutManager

        val dividerItemDecoration = DividerItemDecoration(
            leaderboardRecyclerView.context,
            layoutManager.orientation
        )
        leaderboardRecyclerView.addItemDecoration(dividerItemDecoration)
    }

    /**
     * Opens [UserProfileActivity].
     */
    private fun adapterOnClick(userId: String) {
        val intent = Intent(this, UserProfileActivity::class.java)
        intent.putExtra("USER_ID", userId)
        startActivity(intent)
    }

    /**
     * Opens [AddSubmissionActivity].
     */
    private fun launchAddSubmissionActivity() {
        val intent = Intent(this, AddSubmissionActivity::class.java)
        intent.putExtra("CHALLENGE_ID", challengeId)
        startActivity(intent)
    }

    /**
     * Opens [InviteFriendsActivity].
     */
    private fun launchInviteFriendsActivity() {
        val intent = Intent(this, InviteFriendsActivity::class.java)
        intent.putExtra("CHALLENGE_ID", challengeId)
        startActivity(intent)
    }
}