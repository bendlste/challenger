package cz.cvut.fel.bendlste.challenger.ui.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.model.Badge
import cz.cvut.fel.bendlste.challenger.utils.GlideApp

class BadgesAdapter(private val onClick: (String) -> Unit, private val context: Context) :
    ListAdapter<Badge, BadgesAdapter.ViewHolder>(ChallengeDiffCallback) {

    class ViewHolder(view: View, val onClick: (String) -> Unit, private val context: Context) : RecyclerView.ViewHolder(view) {
        private val imageView: ImageView = view.findViewById(R.id.img_item)
        private val textView: TextView = view.findViewById(R.id.txt_item_name)
        private var currentBadge: Badge? = null

        init {
            // Define click listener for the ViewHolder's View.
            itemView.setOnClickListener {
                currentBadge?.let {
                    onClick(it.challengeId)
                }
            }
        }

        /* Bind user name and profile picture. */
        fun bind(badge: Badge) {
            currentBadge = badge

            textView.text = currentBadge?.challengeName.orEmpty()

            Firebase.storage.reference.child("challenges").child(badge.challengeId).metadata.addOnSuccessListener {
                GlideApp
                    .with(context)
                    .load(Firebase.storage.reference.child("challenges").child(badge.challengeId))
                    .circleCrop()
                    .into(imageView)
            }.addOnFailureListener {
                imageView.setImageResource(R.drawable.default_avatar)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_item, parent, false)
        return ViewHolder(view, onClick, context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val badge = getItem(position)
        holder.bind(badge)
    }
}

object ChallengeDiffCallback : DiffUtil.ItemCallback<Badge>() {
    override fun areItemsTheSame(oldItem: Badge, newItem: Badge): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Badge, newItem: Badge): Boolean {
        return oldItem.challengeId == newItem.challengeId && oldItem.challengeName == newItem.challengeName
    }
}
