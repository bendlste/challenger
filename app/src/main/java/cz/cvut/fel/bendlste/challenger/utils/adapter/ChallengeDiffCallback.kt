package cz.cvut.fel.bendlste.challenger.utils.adapter

import androidx.recyclerview.widget.DiffUtil
import cz.cvut.fel.bendlste.challenger.model.Challenge


object ChallengeDiffCallback : DiffUtil.ItemCallback<Challenge>() {
    override fun areItemsTheSame(oldItem: Challenge, newItem: Challenge): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Challenge, newItem: Challenge): Boolean {
        return oldItem.uuid == newItem.uuid
    }
}