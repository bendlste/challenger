package cz.cvut.fel.bendlste.challenger.viewModel.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.Submission
import cz.cvut.fel.bendlste.challenger.model.repository.ChallengeRepository
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(
    usersRepository: UsersRepository,
    private val challengeRepository: ChallengeRepository
) : ViewModel() {

    /**
     * Challenge ids of current user.
     */
    val challengeIds: LiveData<List<String>> =
        usersRepository.getChallengeIdsOfCurrentUser()

    /**
     * Retrieves [Challenge] objects from challenge [ids].
     */
    fun getChallengesFromIds(ids: List<String>): LiveData<List<Challenge>> =
        challengeRepository.getChallengesFromIds(ids)

    /**
     * Retrieves all submissions of current user.
     */
    fun getSubmissionsForCurrentUser(challengeId: String): LiveData<List<Submission>> {
        val currentUserId = Firebase.auth.currentUser?.uid ?: ""
        return challengeRepository.getSubmissionsOfUser(challengeId, currentUserId)
    }
}