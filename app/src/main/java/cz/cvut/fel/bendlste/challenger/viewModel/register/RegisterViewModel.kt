package cz.cvut.fel.bendlste.challenger.viewModel.register

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val authRepository: AuthRepository
): ViewModel() {

    val name: MutableLiveData<String> = MutableLiveData()
    val emailAddress: MutableLiveData<String> = MutableLiveData()
    val password: MutableLiveData<String> = MutableLiveData()
    val passwordVerify: MutableLiveData<String> = MutableLiveData()

    /**
     * Registers user with email and password.
     */
    fun registerUserWithEmail() {
        authRepository.registerWithEmailAndPassword(emailAddress.value, password.value)
    }
}