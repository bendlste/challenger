package cz.cvut.fel.bendlste.challenger.model

import android.graphics.Bitmap
import com.google.firebase.database.Exclude
import cz.cvut.fel.bendlste.challenger.utils.SubmissionsHelper
import java.io.Serializable

/**
 *  Representing challenge.
 */
data class Challenge(
    val uuid: String,
    val name: String,
    val description: String,
    val bet: String,
    val from: Long,
    val to: Long,
    val type: ChallengeType,
    val targetValue: Int,
    val milestones: List<Int>,
    var isEvaluated: Boolean = false
) : Serializable {

    @Exclude
    var participantsIds: List<String> = emptyList()

    var creator: User? = null

    @get:Exclude
    var picture: Bitmap? = null

    /**
     * Checks if challenge is active.
     *
     * @return true if challenge is active, false otherwise
     */
    @Exclude
    fun isActive(): Boolean {
        val now = System.currentTimeMillis()
        return now in from..to
    }

    /**
     * Checks if challenge is upcoming.
     *
     * @return true if challenge is upcoming, false otherwise
     */
    @Exclude
    fun isUpcoming(): Boolean {
        val now = System.currentTimeMillis()
        return now < from
    }

    /**
     * Checks if challenge is done and should be evaluated.
     *
     * @return true if challenge is done, false otherwise
     */
    @Exclude
    fun isDone(allSubmissions: List<Submission>): Boolean {
        return when (type) {
            ChallengeType.FIRST_TO_THE_FINISH -> evaluateFirstToTheFinish(allSubmissions)
            ChallengeType.LAST_TO_THE_FINISH -> evaluateLastToTheFinish()
            ChallengeType.BIGGEST_COUNT -> evaluateBiggestCount()
        }
    }

    /**
     * Checks if challenge is expired.
     *
     * @return true if challenge is expired, false otherwise
     */
    @Exclude
    fun isExpired(): Boolean {
        val now = System.currentTimeMillis()
        return now > to
    }

    /**
     * Gets winners id.
     *
     * @return winnersId
     */
    @Exclude
    fun getWinnersId(allSubmissions: List<Submission>): String {
        return when (type) {
            ChallengeType.FIRST_TO_THE_FINISH -> getWinnerFirstToFinish(allSubmissions)
            ChallengeType.LAST_TO_THE_FINISH -> getLoserLastToFinish(allSubmissions)
            ChallengeType.BIGGEST_COUNT -> getWinnerBiggestCount(allSubmissions)
        }
    }

    override fun toString(): String {
        return "Challenge(uuid='$uuid', name='$name', description='$description', from=$from, to=$to, type=$type, participantsIds=$participantsIds, creator=$creator)"
    }

    /**
     * Evaluates challenge of type [ChallengeType.FIRST_TO_THE_FINISH].
     */
    private fun evaluateFirstToTheFinish(allSubmissions: List<Submission>): Boolean {
        if (isEvaluated) {
            return false
        }
        for (id in participantsIds) {
            if (SubmissionsHelper.calculateTotalValueForUser(id, allSubmissions) >= targetValue) {
                return true
            }
        }
        return false
    }

    /**
     * Evaluates challenge of type [ChallengeType.LAST_TO_THE_FINISH].
     */
    private fun evaluateLastToTheFinish(): Boolean {
        return isExpired() && !isEvaluated
    }

    /**
     * Evaluates challenge of type [ChallengeType.BIGGEST_COUNT].
     */
    private fun evaluateBiggestCount(): Boolean {
        return isExpired() && !isEvaluated
    }

    /**
     * Gets winner for challenge of type [ChallengeType.FIRST_TO_THE_FINISH].
     */
    private fun getWinnerFirstToFinish(allSubmissions: List<Submission>): String {
        for (id in participantsIds) {
            if (SubmissionsHelper.calculateTotalValueForUser(id, allSubmissions) >= targetValue) {
                return id
            }
        }
        return ""
    }

    /**
     * Gets loser for challenge of type [ChallengeType.LAST_TO_THE_FINISH].
     */
    private fun getLoserLastToFinish(allSubmissions: List<Submission>): String {
        var worst = Int.MAX_VALUE
        var toRet = ""
        for (id in participantsIds) {
            val userTotal = SubmissionsHelper.calculateTotalValueForUser(id, allSubmissions)
            if (userTotal < worst) {
                worst = userTotal
                toRet = id
            }
        }
        return toRet
    }

    /**
     * Gets winner for challenge of type [ChallengeType.BIGGEST_COUNT].
     */
    private fun getWinnerBiggestCount(allSubmissions: List<Submission>): String {
        val userTotals = HashMap<String, Int>()
        for (user in participantsIds) {
            userTotals[user] = SubmissionsHelper.calculateTotalValueForUser(user, allSubmissions)
        }
        val sortedUsers = participantsIds.sortedByDescending { user -> userTotals[user] }
        return sortedUsers[0]
    }

    companion object {
        /**
         * Converts HashMap with proper values to [Challenge].
         */
        @Exclude
        fun convertFromHashMap(hashMap: HashMap<String, Any?>): Challenge {
            val uuid = hashMap["uuid"] as String
            val name = hashMap["name"] as String
            val description = hashMap["description"] as String
            val bet = (hashMap["bet"] as String?) ?: ""
            val from = hashMap["from"] as Long
            val to = hashMap["to"] as Long
            val type = enumValueOf<ChallengeType>(hashMap["type"] as String)
            val targetValue = (hashMap["targetValue"] as Long).toInt()

            val isEvaluated = if (hashMap.containsKey("isEvaluated")) {
                hashMap["isEvaluated"].toString().toBoolean()
            } else false

            // Parse milestones
            val milestonesUnParsed = hashMap["milestones"]
            val milestones = if (milestonesUnParsed != null) {
                milestonesUnParsed as List<Int>
            } else {
                emptyList()
            }

            // Parse participants ids.
            val participantsValue = hashMap["participants"]
            val participantsHashMap = if (participantsValue != null) {
                participantsValue as HashMap<String, String>
            } else {
                HashMap()
            }
            val participantsIds = participantsHashMap.values.toList()

            // Create challenge object.
            val challenge = Challenge(uuid, name, description, bet, from, to, type, targetValue, milestones, isEvaluated)
            challenge.participantsIds = participantsIds

            return challenge
        }
    }
}