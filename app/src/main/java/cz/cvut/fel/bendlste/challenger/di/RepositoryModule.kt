package cz.cvut.fel.bendlste.challenger.di

import android.content.Context
import cz.cvut.fel.bendlste.challenger.App
import cz.cvut.fel.bendlste.challenger.model.repository.AuthRepository
import cz.cvut.fel.bendlste.challenger.model.repository.ChallengeRepository
import cz.cvut.fel.bendlste.challenger.model.repository.FriendsActivityRepository
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    fun provideAuthRepository() = AuthRepository()

    @Provides
    fun provideUsersRepository(@ApplicationContext applicationContext: Context) = UsersRepository(applicationContext)

    @Provides
    fun provideChallengeRepository() = ChallengeRepository()

    @Provides
    fun provideFriendsActivityRepository() = FriendsActivityRepository()
}