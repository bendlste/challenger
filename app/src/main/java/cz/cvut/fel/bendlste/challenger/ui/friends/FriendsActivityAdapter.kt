package cz.cvut.fel.bendlste.challenger.ui.friends

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.model.FriendsActivity
import cz.cvut.fel.bendlste.challenger.utils.CurrentUserHelper.currentUserId
import cz.cvut.fel.bendlste.challenger.utils.GlideApp

class FriendsActivityAdapter(
    private val onClick: (String) -> Unit,
    private val onLike: (String) -> Unit,
    private val onDislike: (String) -> Unit,
    private val context: Context) :
    ListAdapter<FriendsActivity, FriendsActivityAdapter.ViewHolder>(FriendsActivityDiffCallback) {

    class ViewHolder(view: View,
                     private val onClick: (String) -> Unit,
                     private val onLike: (String) -> Unit,
                     private val onDislike: (String) -> Unit,
                     private val context: Context) : RecyclerView.ViewHolder(view) {
        private val imageView: ImageView = view.findViewById(R.id.fai_image)
        private val textTitle: TextView = view.findViewById(R.id.fai_title)
        private val textSubtitle: TextView = view.findViewById(R.id.fai_subtitle)
        private val textComment: TextView = view.findViewById(R.id.fai_comment)
        private val btnLike: Button = view.findViewById(R.id.fai_btn_like)
        private val btnDislike: Button = view.findViewById(R.id.fai_btn_dislike)
        private lateinit var currentItem: FriendsActivity

        init {
            // Define click listener for the ViewHolder's View.
            itemView.setOnClickListener {
                onClick(currentItem.challengeId)
            }
            btnLike.setOnClickListener {
                onLike(currentItem.uuid)
                currentItem.upVotes[currentUserId] = currentUserId
                bind(currentItem)
            }
            btnDislike.setOnClickListener {
                onDislike(currentItem.uuid)
                currentItem.downVotes[currentUserId] = currentUserId
                bind(currentItem)
            }
        }

        /* Bind attributes. */
        fun bind(friendsActivity: FriendsActivity) {
            currentItem = friendsActivity

            // If friendsActivity is winnerAnnouncment, handle it here
            if (currentItem.isWinnerAnnouncment || currentItem.addedValue == -1) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    textTitle.text = Html.fromHtml(
                        "${currentItem.userName} has won <font color=\"#ff7129\">" +
                                currentItem.challengeName + "</font> challenge.", HtmlCompat.FROM_HTML_MODE_LEGACY)
                } else {
                    textTitle.text = Html.fromHtml(
                        "${currentItem.userName} has won <font color=\"#ff7129\">" +
                                currentItem.challengeName + "</font> challenge.")
                }

                textSubtitle.visibility = View.GONE
                textComment.visibility = View.GONE

                Firebase.storage.reference.child("challenges").child(currentItem.challengeId).metadata.addOnSuccessListener {
                    GlideApp
                        .with(context)
                        .load(Firebase.storage.reference.child("challenges").child(currentItem.challengeId))
                        .into(imageView)
                }.addOnFailureListener {
                    imageView.setImageResource(R.drawable.default_challenge)
                }
                return
            }

            // Handle non-winner-announcment friend activities
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                textTitle.text = Html.fromHtml(
                    "${currentItem.userName} added ${currentItem.addedValue} units to " +
                            "<font color=\"#ff7129\">" + currentItem.challengeName + "</font>" + " challenge.", HtmlCompat.FROM_HTML_MODE_LEGACY
                )
            } else {
                textTitle.text = Html.fromHtml(
                    "${currentItem.userName} added ${currentItem.addedValue} units to " +
                            "<font color=\"#ff7129\">" + currentItem.challengeName + "</font>" + " challenge."
                )
            }

            // Check for milestones
            if (currentItem.isM1Reached() && !currentItem.isM2Reached()) {
                textSubtitle.text = Html.fromHtml(
                    "${currentItem.userName} has already passed " +
                            "<font color=\"#ff7129\">first milestone</font>.")
            }

            else if (currentItem.isM2Reached()) {
                textSubtitle.text = Html.fromHtml(
                    "${currentItem.userName} has already passed " +
                            "<font color=\"#ff7129\">second milestone</font>.")
            } else {
                textSubtitle.visibility = View.GONE
            }

            // Check for upVotes
            if (currentItem.upVotes.containsKey(currentUserId) || currentItem.downVotes.containsKey(currentUserId)) {
                val errorMessage = "Sorry, you have already submitted your reaction."
                btnLike.setOnClickListener {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
                }
                btnDislike.setOnClickListener {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show()
                }
            }

            textComment.text = friendsActivity.comment

            btnLike.text = "Like (${currentItem.upVotes.size})"
            btnDislike.text = "Dislike (${currentItem.downVotes.size})"

            imageView.visibility = View.GONE

            Firebase.storage.reference.child("friends_activity").child(currentItem.uuid).metadata.addOnSuccessListener {
                GlideApp
                    .with(context)
                    .load(Firebase.storage.reference.child("friends_activity").child(currentItem.uuid))
                    .into(imageView)
                imageView.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.friends_activity_adapter_item, parent, false)
        return ViewHolder(view, onClick, onLike, onDislike, context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val challenge = getItem(position)
        holder.bind(challenge)
    }
}

object FriendsActivityDiffCallback : DiffUtil.ItemCallback<FriendsActivity>() {
    override fun areItemsTheSame(oldItem: FriendsActivity, newItem: FriendsActivity): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: FriendsActivity, newItem: FriendsActivity): Boolean {
        return oldItem.uuid == newItem.uuid
    }
}