package cz.cvut.fel.bendlste.challenger.viewModel.explore

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.repository.ChallengeRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ExploreViewModel @Inject constructor(private val challengeRepository: ChallengeRepository) : ViewModel() {

    /**
     * Retrieves all [Challenge] objects from DB.
     */
    val allChallenges: LiveData<List<Challenge>>
        get() = challengeRepository.getAllChallenges()
}
