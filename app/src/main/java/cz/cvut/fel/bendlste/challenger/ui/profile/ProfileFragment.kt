package cz.cvut.fel.bendlste.challenger.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.databinding.ProfileFragmentBinding
import cz.cvut.fel.bendlste.challenger.model.User
import cz.cvut.fel.bendlste.challenger.ui.LoginActivity
import cz.cvut.fel.bendlste.challenger.ui.explore.ChallengeDetailActivity
import cz.cvut.fel.bendlste.challenger.ui.friends.UserProfileActivity
import cz.cvut.fel.bendlste.challenger.utils.CurrentUserHelper.currentUserId
import cz.cvut.fel.bendlste.challenger.utils.GlideApp
import cz.cvut.fel.bendlste.challenger.viewModel.profile.FriendsViewModel
import cz.cvut.fel.bendlste.challenger.viewModel.profile.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileFragment(
) : Fragment(R.layout.profile_fragment) {

    private lateinit var friendsViewModel: FriendsViewModel
    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var friendsAdapter: FriendsAdapter
    private lateinit var badgesAdapter: BadgesAdapter

    private var _binding: ProfileFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = ProfileFragmentBinding.bind(view)
        friendsViewModel = ViewModelProvider(this).get(FriendsViewModel::class.java)
        profileViewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        setUpAdapters()

        fetchData()

        setUpUI()

        setOnClickListeners()
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    /**
     * Sets up [friendsAdapter] and [badgesAdapter].
     */
    private fun setUpAdapters() {
        friendsAdapter = FriendsAdapter({ user -> friendsAdapterOnClick(user) }, requireContext())
        val friendRecyclerView: RecyclerView = binding.friendsRecyclerView

        friendRecyclerView.adapter = friendsAdapter

        val layoutManagerFriends = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        friendRecyclerView.layoutManager = layoutManagerFriends

        badgesAdapter = BadgesAdapter({ challenge -> badgesAdapterOnClick(challenge) }, requireContext())
        val badgesRecyclerView = binding.badgesRecyclerView
        badgesRecyclerView.adapter = badgesAdapter

        val layoutManagerBadges = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        badgesRecyclerView.layoutManager = layoutManagerBadges
    }

    /**
     * Retrieves data from Firebase database and passes them to adapters.
     */
    private fun fetchData() {
        // Friends fetch.
        friendsViewModel.friendsIdsLiveData.observe(viewLifecycleOwner, { ids ->
            friendsViewModel.getFriendsFromIds(ids).observe(viewLifecycleOwner, {
                if (it.isNotEmpty()) {
                    friendsAdapter.submitList(it)
                }
            })
        })

        // Badges fetch.
        friendsViewModel.badges.observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                badgesAdapter.submitList(it)
            }
        })
    }

    /**
     * Sets up UI with data from Firebase database and Firebase storage.
     */
    private fun setUpUI() {
        val nameObserver = Observer<User?> { user ->
            binding.txtName.text = user.name
        }

        val storageRef = Firebase.storage.reference.child("profile_pictures").child(currentUserId)

        storageRef.metadata.addOnSuccessListener {
            GlideApp
                .with(this)
                .load(storageRef)
                .circleCrop()
                .into(binding.imgProfilePicture)
        }.addOnFailureListener {
            binding.imgProfilePicture.setImageResource(R.drawable.default_avatar)
        }

        profileViewModel.currentUser.observe(this.requireActivity(), nameObserver)
    }

    /**
     * Launches [UserProfileActivity].
     */
    private fun friendsAdapterOnClick(user: User) {
        val intent = Intent(this.requireContext(), UserProfileActivity::class.java)
        intent.putExtra("USER_ID", user.uuid)
        startActivity(intent)
    }

    /**
     * Launches [ChallengeDetailActivity].
     */
    private fun badgesAdapterOnClick(challengeId: String) {
        val intent = Intent(this.requireContext(), ChallengeDetailActivity::class.java)
        intent.putExtra("CHALLENGE_ID", challengeId)
        startActivity(intent)
    }

    /**
     * Sets click listener for logout button.
     */
    private fun setOnClickListeners() {
        binding.btnLogOut.setOnClickListener {
            Toast.makeText(this.requireContext(), "Logged out", Toast.LENGTH_SHORT).show()
            profileViewModel.logOut()
            goToLoginActivity()
        }
    }

    /**
     * Launches [LoginActivity].
     */
    private fun goToLoginActivity() {
        val intent = Intent(this.requireActivity(), LoginActivity::class.java)
        startActivity(intent)
        requireActivity().finish()
    }
}