package cz.cvut.fel.bendlste.challenger.viewModel.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.User
import cz.cvut.fel.bendlste.challenger.model.repository.AuthRepository
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val usersRepository: UsersRepository,
    private val authRepository: AuthRepository
): ViewModel() {

    /**
     * Current [User] object.
     */
    val currentUser: LiveData<User?>
        get() {
            val toRet = usersRepository.getCurrentUserAsUser()
            requireNotNull(toRet) {
                "User should not be null. You should not retrieve current user when no one is logged in."
            }
            return toRet
        }

    /**
     * Logs out current user.
     */
    fun logOut() {
        authRepository.logOut()
    }
}