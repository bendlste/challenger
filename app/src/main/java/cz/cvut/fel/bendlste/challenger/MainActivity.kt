package cz.cvut.fel.bendlste.challenger

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomnavigation.BottomNavigationView
import cz.cvut.fel.bendlste.challenger.databinding.ActivityMainBinding
import cz.cvut.fel.bendlste.challenger.model.FriendsActivity
import cz.cvut.fel.bendlste.challenger.ui.dashboard.DashboardFragment
import cz.cvut.fel.bendlste.challenger.model.User
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import cz.cvut.fel.bendlste.challenger.ui.explore.ChallengeDetailActivity
import cz.cvut.fel.bendlste.challenger.ui.explore.ExploreFragment
import cz.cvut.fel.bendlste.challenger.ui.friends.FriendsActivityFragment
import cz.cvut.fel.bendlste.challenger.ui.profile.ProfileFragment
import cz.cvut.fel.bendlste.challenger.viewModel.MainActivityViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mainActivityViewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.hide()

        mainActivityViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setMenuListener()

        if (savedInstanceState == null) {
            val fragment = DashboardFragment()
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment, fragment.javaClass.simpleName)
                .commit()
        }

        /**
         * Handles friends additions.
         */
        mainActivityViewModel.friendsAdditions.observe(this, {
            if (!it.isNullOrEmpty()) {
                val notificationBody = when (it.size) {
                    1 -> "${it[0]}"
                    2 -> "${it[1]} and ${it[2]}"
                    else -> "${it[1]}, ${it[2]} and ${it.size - 2} others"
                }
                sendNewFriendNotification(notificationBody)
            }
        })

        /**
         * Handles challenge invites.
         */
        mainActivityViewModel.challengeInvites.observe(this, {hashMap ->
            if (!hashMap.isNullOrEmpty()) {
                for (item in hashMap.keys) {
                    sendChallengeInviteNotification(item, hashMap[item].orEmpty())
                    mainActivityViewModel.removeChallengeInvites()
                }
            }
        })

        /**
         * Handles challenge winner evaluation.
         */
        mainActivityViewModel.checkForDoneChallenges().observe(this, { challenges ->
            if (challenges != null) {
                for (item in challenges) {
                    mainActivityViewModel.getAllSubmissionsForChallenge(item.uuid).observe(this, { allSubmissions ->
                        if (item.isDone(allSubmissions)) {
                            mainActivityViewModel.markChallengeAsEvaluated(item.uuid)
                            val winnersId = item.getWinnersId(allSubmissions)
                            mainActivityViewModel.getUserById(winnersId).observe(this, { winner ->
                              if (winner != null) {
                                  val activity = FriendsActivity(
                                      challengeId = item.uuid,
                                      challengeName = item.name,
                                      userId = winnersId,
                                      userName = winner.name.orEmpty(),
                                      addedValue = -1,
                                      sumValues = -1,
                                      m1 = item.milestones[0],
                                      m2 = item.milestones[1],
                                      upVotes = HashMap(),
                                      downVotes = HashMap(),
                                      comment = ""
                                  ).apply {
                                      isWinnerAnnouncment = true
                                  }
                                  mainActivityViewModel.createFriendsActivity(activity)
                              }
                            })
                        }
                    })
                }
            }
            mainActivityViewModel.checkForDoneChallenges().removeObservers(this)
        })
    }

    /**
     * Handle bottom navigation.
     */
    private fun setMenuListener() {
        val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_dashboard -> {
                    val fragment = DashboardFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, fragment, fragment.javaClass.simpleName)
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.action_authentication -> {
                    val fragment = ExploreFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, fragment, fragment.javaClass.simpleName)
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.action_activity -> {
                    val fragment = FriendsActivityFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, fragment, fragment.javaClass.simpleName)
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.action_profile -> {
                    val fragment = ProfileFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, fragment, fragment.javaClass.simpleName)
                        .commit()
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

        binding.bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    /**
     * Sends new friend notification.
     */
    private fun sendNewFriendNotification(bodyText: String) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setContentTitle("New friend!")
            .setContentText("User $bodyText has added you to the friend list. Go and check them out.")
            .setSmallIcon(R.drawable.default_avatar)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())

    }

    /**
     * Sends challenge invite notification.
     */
    private fun sendChallengeInviteNotification(challengeId: String, challengeName: String) {
        val intent = Intent(this, ChallengeDetailActivity::class.java)
        intent.putExtra("CHALLENGE_ID", challengeId)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setContentTitle("Challenge invite!")
            .setContentText("You have been invited to $challengeName challenge. Go and check it out.")
            .setSmallIcon(R.mipmap.icon)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }
}
