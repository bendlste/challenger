package cz.cvut.fel.bendlste.challenger.viewModel.explore

import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.repository.ChallengeRepository
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CreateChallengeViewModel
@Inject constructor(
    private val challengeRepository: ChallengeRepository,
    private val usersRepository: UsersRepository
) : ViewModel() {

    /**
     * Adds [challenge] to the DB.
     */
    fun addChallenge(challenge: Challenge) {
        challengeRepository.addChallenge(challenge)
    }

    /**
     * Adds current user to [Challenge] with id [challengeId].
     */
    fun joinChallenge(challengeId: String) {
        usersRepository.addChallenge(challengeId)
        challengeRepository.addParticipant(challengeId)
    }
}