package cz.cvut.fel.bendlste.challenger.utils

import cz.cvut.fel.bendlste.challenger.model.Submission

object SubmissionsHelper {
    fun calculateTotalValueForUser(userId: String, submissions: List<Submission>): Int {
        val filtered = submissions.filter { it.userId == userId }
        return filtered.sumBy { it.value }
    }
}