package cz.cvut.fel.bendlste.challenger.viewModel.friends

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class UserProfileViewModel @Inject constructor(private val usersRepository: UsersRepository) : ViewModel() {

    /**
     * Friend ids of current user.
     */
    val friendIds: LiveData<List<String>>
        get() = usersRepository.getFriendsIds()

    /**
     * Retrieves badges of user with id [userId].
     */
    fun getBadges(userId: String) = usersRepository.getBadgesOfUser(userId)

    /**
     * Adds user with id [friendId] to current user's friend list.
     * Adds a friend addition entry.
     */
    fun addFriend(friendId: String) {
        usersRepository.addFriend(friendId)
        usersRepository.addFriendAdditionEntry(friendId)
    }

    /**
     * Returns User object based on [userId].
     */
    fun getUser(userId: String) = usersRepository.getUserById(userId)
}