package cz.cvut.fel.bendlste.challenger.ui.explore

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import cz.cvut.fel.bendlste.challenger.databinding.ActivityAddSubmissionBinding
import cz.cvut.fel.bendlste.challenger.model.Badge
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.FriendsActivity
import cz.cvut.fel.bendlste.challenger.model.Submission
import cz.cvut.fel.bendlste.challenger.utils.CurrentUserHelper
import cz.cvut.fel.bendlste.challenger.utils.CurrentUserHelper.currentUserId
import cz.cvut.fel.bendlste.challenger.utils.GlideApp
import cz.cvut.fel.bendlste.challenger.utils.SubmissionsHelper
import cz.cvut.fel.bendlste.challenger.viewModel.explore.AddSubmissionViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.io.FileNotFoundException
import java.io.IOException


const val GALLERY_CODE = 123
const val CAMERA_CODE = 234
const val CAMERA_PERMISSION_CODE = 345

@AndroidEntryPoint
class AddSubmissionActivity : AppCompatActivity() {

    private lateinit var addSubmissionViewModel: AddSubmissionViewModel
    private lateinit var binding: ActivityAddSubmissionBinding
    private lateinit var challengeId: String
    private var challenge: Challenge? = null
    private lateinit var submissionsOfCurrentUser: List<Submission>
    private var image: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.hide()

        binding = ActivityAddSubmissionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        addSubmissionViewModel = ViewModelProvider(this).get(AddSubmissionViewModel::class.java)

        getChallengeDetailsFromIntent()
        fetchData()
        setOnClickListeners()
    }

    /**
     * Retrieves data from Firebase Database.
     */
    private fun fetchData() {
        addSubmissionViewModel.getSubmissionsOfUser(challengeId).observe(this, {
            submissionsOfCurrentUser = it
        })

        addSubmissionViewModel.getChallenge(challengeId).observe(this, {
            challenge = it
        })
    }

    /**
     * Retrieves challenge id from intent.
     */
    private fun getChallengeDetailsFromIntent() {
        val challengeIdIntent = intent.getSerializableExtra("CHALLENGE_ID")
        challengeId = if (challengeIdIntent is String) {
            challengeIdIntent
        } else ""
    }

    /**
     * Sets click listeners for buttons.
     */
    private fun setOnClickListeners() {
        binding.asBtnSelectPicture.setOnClickListener { getImageFromGallery() }
        binding.asBtnTakePicture.setOnClickListener { requestCameraPermission() }
        binding.btnAdd.setOnClickListener { submit() }
    }

    /**
     * Used for adding pictures to submissions.
     */
    // Copy-pasted from CreateChallengeActivity
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY_CODE && resultCode == Activity.RESULT_OK) {
            val selectedImage = data?.data
            try {
                image = MediaStore.Images.Media.getBitmap(applicationContext.contentResolver, selectedImage)
                GlideApp.with(applicationContext)
                    .load(selectedImage)
                    .fitCenter()
                    .into(binding.asImage)

            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        if (requestCode == CAMERA_CODE && resultCode == RESULT_OK) {
            image = data?.extras?.get("data") as Bitmap?
            binding.asImage.setImageBitmap(image)
        }

    }

    /**
     * Opens gallery and lets user select photo.
     */
    private fun getImageFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_CODE)
    }

    /**
     * Requests for gallery and camera permission.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show()
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_CODE)
            } else {
                Toast.makeText(this, "Camera permission denied.", Toast.LENGTH_LONG).show()
            }
        }
    }

    /**
     * Requests camera permission and starts camera.
     */
    private fun requestCameraPermission() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, CAMERA_CODE)
    }

    /**
     * Submits submission and friends activity to Database.
     * Checks if user gained any badge.
     */
    private fun submit() {
        if (binding.asValue.text.isBlank()) {
            Toast.makeText(this, "Please enter valid value.", Toast.LENGTH_SHORT).show()
            return
        }

        val value = binding.asValue.text.toString().toInt()
        if (value <= 0) {
            Toast.makeText(this, "Please enter valid value.", Toast.LENGTH_SHORT).show()
            return
        }

        val comment = binding.asComment.text.toString()

        val submission = Submission(challengeId, currentUserId, value, System.currentTimeMillis())
        val totalValue = SubmissionsHelper.calculateTotalValueForUser(currentUserId, submissionsOfCurrentUser) + value

        val ch = challenge
        if (ch == null) {
            Toast.makeText(this, "Data not initialized properly. Please check your connection.", Toast.LENGTH_SHORT).show()
            return
        }

        val fa = FriendsActivity(
            challengeId = challengeId,
            userId = currentUserId,
            challengeName = challenge?.name.orEmpty(),
            userName = CurrentUserHelper.currentUserName,
            addedValue = value,
            sumValues = totalValue,
            m1 = ch.milestones[0],
            m2 = ch.milestones[1],
            upVotes = HashMap(),
            downVotes = HashMap(),
            comment = comment
        )
        fa.image = image

        addSubmissionViewModel.addSubmission(submission)
        addSubmissionViewModel.createFriendsActivity(fa)
        if (totalValue >= ch.targetValue) {
            val badge = Badge(challengeId, ch.name)
            addSubmissionViewModel.addBadge(badge)
        }

        finish()
    }
}