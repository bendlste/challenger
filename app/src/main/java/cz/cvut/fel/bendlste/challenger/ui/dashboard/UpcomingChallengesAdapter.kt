package cz.cvut.fel.bendlste.challenger.ui.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.utils.GlideApp
import cz.cvut.fel.bendlste.challenger.utils.adapter.ChallengeDiffCallback

class UpcomingChallengesAdapter(
    private val onClick: (Challenge) -> Unit,
    private val context: Context
) :
    ListAdapter<Challenge, UpcomingChallengesAdapter.ViewHolder>(ChallengeDiffCallback) {

    class ViewHolder(
        view: View, val onClick: (Challenge) -> Unit,
        private val context: Context
    ) : RecyclerView.ViewHolder(view) {
        private val imageView: ImageView = view.findViewById(R.id.da_image)
        private val textName: TextView = view.findViewById(R.id.da_name)
        private val total: TextView = view.findViewById(R.id.da_total)
        private var currentChallenge: Challenge? = null

        init {
            // Define click listener for the ViewHolder's View.
            itemView.setOnClickListener {
                currentChallenge?.let {
                    onClick(it)
                }
            }
        }

        /* Bind challenge name, total and picture. */
        fun bind(challenge: Challenge) {
            currentChallenge = challenge

            val storageRef = Firebase.storage.reference.child("challenges").child(challenge.uuid)

            storageRef.metadata.addOnSuccessListener {
                GlideApp
                    .with(context)
                    .load(storageRef)
                    .into(imageView)
            }.addOnFailureListener {
                imageView.setImageResource(R.drawable.default_challenge)
            }

            textName.text = currentChallenge?.name.orEmpty()
            total.visibility = View.GONE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.dashboard_adapter_item, parent, false)
        return ViewHolder(view, onClick, context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val challenge = getItem(position)
        holder.bind(challenge)
    }
}
