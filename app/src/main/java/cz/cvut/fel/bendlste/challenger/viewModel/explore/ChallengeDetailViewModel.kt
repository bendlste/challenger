package cz.cvut.fel.bendlste.challenger.viewModel.explore

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.Submission
import cz.cvut.fel.bendlste.challenger.model.User
import cz.cvut.fel.bendlste.challenger.model.repository.ChallengeRepository
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ChallengeDetailViewModel
@Inject constructor(
    private val challengeRepository: ChallengeRepository,
    private val usersRepository: UsersRepository
) : ViewModel() {

    /**
     * Current [User] object.
     */
    val currentUser: LiveData<User?>?
        get() = usersRepository.getCurrentUserAsUser()

    /**
     * Retrieves [Challenge] object from [challengeId].
     */
    fun getChallenge(challengeId: String): LiveData<Challenge?> {
        return challengeRepository.getChallengeById(challengeId)
    }

    /**
     * Retrieves [User] objects from [participantIds].
     */
    fun getParticipants(participantIds: List<String>): LiveData<List<User>> {
        return usersRepository.getUsersByIds(participantIds)
    }

    /**
     * Retrieves all [Submission]s for [Challenge] with id [challengeId].
     */
    fun getAllSubmissionsForChallenge(challengeId: String): LiveData<List<Submission>> {
        return challengeRepository.getAllSubmissionsForChallenge(challengeId)
    }

    /**
     * Adds [currentUser] to challenge with id [challengeId].
     */
    fun joinChallenge(challengeId: String) {
        usersRepository.addChallenge(challengeId)
        challengeRepository.addParticipant(challengeId)
    }
}