package cz.cvut.fel.bendlste.challenger.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.AuthCredential
import cz.cvut.fel.bendlste.challenger.model.repository.AuthRepository
import cz.cvut.fel.bendlste.challenger.model.User
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : ViewModel() {

    var authenticatedUserLiveData: LiveData<User>? = null
    var createdUserLiveData: LiveData<User>? = null


    val emailAddress: MutableLiveData<String> = MutableLiveData()
    val password: MutableLiveData<String> = MutableLiveData()

    /**
     * Signs in user with Google
     */
    fun signInWithGoogle(googleAuthCredential: AuthCredential?) {
        authenticatedUserLiveData = authRepository.firebaseSignInWithGoogle(googleAuthCredential)
    }

    /**
     * Creates user in DB.
     */
    fun createUser(authenticatedUser: User) {
        createdUserLiveData = authRepository.createUserInDatabaseIfNotExists(authenticatedUser)
    }

    /**
     * Signs in user with email and password.
     */
    fun signInWithEmailAndPassword() {
        val password = password.value
        val email = emailAddress.value
        if (!password.isNullOrEmpty() || !email.isNullOrEmpty()) {
            authenticatedUserLiveData = authRepository.signUpWithEmailAndPassword(email!!, password!!)
        }
    }
}