package cz.cvut.fel.bendlste.challenger.viewModel.friends

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.User
import cz.cvut.fel.bendlste.challenger.model.repository.FriendsActivityRepository
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FriendsActivityViewModel @Inject constructor(
    private val friendsActivityRepository: FriendsActivityRepository,
    private val usersRepository: UsersRepository
) : ViewModel() {

    /**
     * Friend ids of current user.
     */
    val friendIds: LiveData<List<String>>
        get() = usersRepository.getFriendsIds()

    /**
     * Retrieves all friends activities for current user.
     */
    fun fetchFriendsActivity(friendIds: List<String>) = friendsActivityRepository.getFriendsActivityForUser(friendIds)

    /**
     * Retrieves [User] based on [name].
     */
    fun getUserByName(name: String): LiveData<User?> = usersRepository.getUserByName(name)

    /**
     * Adds like to friends activity with id [friendsActivityId].
     */
    fun like(friendsActivityId: String) = friendsActivityRepository.like(friendsActivityId)

    /**
     * Adds dislike to friends activity with id [friendsActivityId].
     */
    fun dislike(friendsActivityId: String) = friendsActivityRepository.dislike(friendsActivityId)
}