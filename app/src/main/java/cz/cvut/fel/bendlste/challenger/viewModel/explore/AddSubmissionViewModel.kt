package cz.cvut.fel.bendlste.challenger.viewModel.explore

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.Badge
import cz.cvut.fel.bendlste.challenger.model.FriendsActivity
import cz.cvut.fel.bendlste.challenger.model.Submission
import cz.cvut.fel.bendlste.challenger.model.repository.ChallengeRepository
import cz.cvut.fel.bendlste.challenger.model.repository.FriendsActivityRepository
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import cz.cvut.fel.bendlste.challenger.utils.CurrentUserHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AddSubmissionViewModel @Inject constructor(
    private val friendsActivityRepository: FriendsActivityRepository,
    private val challengeRepository: ChallengeRepository,
    private val usersRepository: UsersRepository
) : ViewModel() {

    /**
     * Adds [submission] to the DB.
     */
    fun addSubmission(submission: Submission) = challengeRepository.addSubmission(submission)

    /**
     * Adds [friendsActivity] to the DB. Adds [FriendsActivity.image] to Firebase storage.
     */
    fun createFriendsActivity(friendsActivity: FriendsActivity) {
        friendsActivityRepository.createFriendsActivity(friendsActivity)
        friendsActivityRepository.submitFriendsActivityPicture(friendsActivity)
    }

    /**
     * Retrieves submissions of current user for challenge with id [challengeId].
     */
    fun getSubmissionsOfUser(challengeId: String): LiveData<List<Submission>> =
        challengeRepository.getSubmissionsOfUser(challengeId, CurrentUserHelper.currentUserId)

    /**
     * Retrieves Challenge object from [challengeId].
     */
    fun getChallenge(challengeId: String) = challengeRepository.getChallengeById(challengeId)

    /**
     * Adds [badge] to current user.
     */
    fun addBadge(badge: Badge) = usersRepository.addBadge(badge)
}
