package cz.cvut.fel.bendlste.challenger.ui.explore

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.model.Submission
import cz.cvut.fel.bendlste.challenger.model.User
import cz.cvut.fel.bendlste.challenger.utils.GlideApp
import cz.cvut.fel.bendlste.challenger.utils.SubmissionsHelper

class LeaderboardAdapter(
    private val onClick: (String) -> Unit,
    private val context: Context,
    private val userSubmissions: HashMap<User, List<Submission>>
) : ListAdapter<User, LeaderboardAdapter.ViewHolder>(UserDiffCallback) {

    class ViewHolder(
        view: View,
        val onClick: (String) -> Unit,
        private val context: Context,
        private val userSubmissions: HashMap<User, List<Submission>>
        ) : RecyclerView.ViewHolder(view) {
        private val imageView: ImageView = view.findViewById(R.id.la_image)
        private val name: TextView = view.findViewById(R.id.la_name)
        private val total: TextView = view.findViewById(R.id.la_total)
        private val positionView: TextView = view.findViewById(R.id.la_position)
        private var currentUser: User? = null

        init {
            // Define click listener for the ViewHolder's View.
            itemView.setOnClickListener {
                currentUser?.let {
                    onClick(it.uuid)
                }
            }
        }

        /* Bind challenge name and picture. */
        fun bind(user: User, position: Int) {
            if (userSubmissions.containsKey(user)) {
                val distinct = userSubmissions[user]?.distinctBy { submission -> submission.timestamp }
                val totalValue = distinct?.let { SubmissionsHelper.calculateTotalValueForUser(user.uuid, it) }
                total.text = totalValue.toString()
            } else {
                return
            }

            currentUser = user

            positionView.text = (position + 1).toString()

            name.text = currentUser?.name.orEmpty()

            val storageRef = Firebase.storage.reference.child("profile_pictures").child(user.uuid)

            storageRef.metadata.addOnSuccessListener {
                GlideApp.with(context)
                    .load(storageRef)
                    .circleCrop()
                    .into(imageView)
            }.addOnFailureListener {
                imageView.setImageResource(R.drawable.default_avatar)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.leaderboard_adapter_item, parent, false)
        return ViewHolder(view, onClick, context, userSubmissions)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = getItem(position)
        holder.bind(user, position)
    }

}

object UserDiffCallback : DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.uuid == newItem.uuid
    }
}