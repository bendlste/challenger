package cz.cvut.fel.bendlste.challenger.ui.explore

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.VectorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.databinding.ExploreFragmentBinding
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.ChallengeType
import cz.cvut.fel.bendlste.challenger.utils.DefaultImageHelper
import cz.cvut.fel.bendlste.challenger.viewModel.explore.ExploreViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit


@AndroidEntryPoint
class ExploreFragment : Fragment(R.layout.explore_fragment) {

    private var _binding: ExploreFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var exploreViewModel: ExploreViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = ExploreFragmentBinding.bind(view)
        exploreViewModel = ViewModelProvider(this).get(ExploreViewModel::class.java)

        setClickListeners()
        setUpUI()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Sets up click listeners for buttons.
     */
    private fun setClickListeners() {
        binding.floatingActionButton.setOnClickListener { openCreateChallengeActivity() }
    }

    /**
     * Sets up adapter.
     * Passes fetched data to adapter.
     */
    private fun setUpUI() {
        val exploreAdapter = ExploreAdapter ({ challenge -> adapterOnClick(challenge) }, requireContext())
        val exploreRecyclerView: RecyclerView = binding.exploreRecyclerView

        exploreRecyclerView.adapter = exploreAdapter

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        exploreRecyclerView.layoutManager = layoutManager

        val dividerItemDecoration = DividerItemDecoration(
            exploreRecyclerView.context,
            layoutManager.orientation
        )
        exploreRecyclerView.addItemDecoration(dividerItemDecoration)

        exploreViewModel.allChallenges.observe(viewLifecycleOwner, { list ->
            exploreAdapter.submitList(list.filter { challenge -> !challenge.isExpired() })
        })
    }

    /**
     * Launches [ChallengeDetailActivity].
     */
    private fun adapterOnClick(challenge: Challenge) {
        val intent = Intent(this.requireContext(), ChallengeDetailActivity::class.java)
        intent.putExtra("CHALLENGE_ID", challenge.uuid)
        startActivity(intent)
    }

    /**
     * Launches [CreateChallengeActivity].
     */
    private fun openCreateChallengeActivity() {
        val intent = Intent(this.requireActivity(), CreateChallengeActivity::class.java)
        startActivity(intent)
    }
}