package cz.cvut.fel.bendlste.challenger.utils

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.VectorDrawable
import androidx.core.graphics.drawable.toBitmap
import cz.cvut.fel.bendlste.challenger.R

object DefaultImageHelper {
    fun getDefaultChallengeImage(resources: Resources): Bitmap {
        val drawable = resources.getDrawable(R.drawable.default_challenge, resources.newTheme())
        return (drawable as VectorDrawable).toBitmap()
    }

    fun getDefaultProfileImage(resources: Resources): Bitmap {
        val drawable = resources.getDrawable(R.drawable.default_avatar, resources.newTheme())
        return (drawable as VectorDrawable).toBitmap()
    }
}