package cz.cvut.fel.bendlste.challenger.model

import com.google.firebase.database.Exclude

/**
 * Representing Badge.
 */
data class Badge(
    val challengeId: String,
    val challengeName: String
) {
    companion object {
        /**
         * Converts HashMap with proper values to [Badge].
         */
        @Exclude
        fun convertFromHashMap(hashMap: HashMap<String, String>): List<Badge> {
            val toRet = mutableListOf<Badge>()
            for (key in hashMap.keys) {
                toRet.add(Badge(key, hashMap[key].orEmpty()))
            }
            return toRet
        }
    }
}