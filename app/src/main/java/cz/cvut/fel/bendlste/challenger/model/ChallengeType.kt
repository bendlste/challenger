package cz.cvut.fel.bendlste.challenger.model

/**
 * Types of challenges.
 */
enum class ChallengeType {
    FIRST_TO_THE_FINISH,
    LAST_TO_THE_FINISH,
    BIGGEST_COUNT
//    BEST_PERFORMANCE
}