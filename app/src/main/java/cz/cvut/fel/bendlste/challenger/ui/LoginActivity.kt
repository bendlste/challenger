package cz.cvut.fel.bendlste.challenger.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import cz.cvut.fel.bendlste.challenger.MainActivity
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.databinding.ActivityLoginBinding
import cz.cvut.fel.bendlste.challenger.model.User
import cz.cvut.fel.bendlste.challenger.ui.register.RegisterActivity
import cz.cvut.fel.bendlste.challenger.ui.register.SetUserDetailsActivity
import cz.cvut.fel.bendlste.challenger.viewModel.AuthViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity: AppCompatActivity() {
    private lateinit var authViewModel: AuthViewModel

    private lateinit var binding: ActivityLoginBinding

    private lateinit var googleSignInClient: GoogleSignInClient

    private val RC_SIGN_IN = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        if (Firebase.auth.currentUser != null) {
            goToMainActivity()
        }

        super.onCreate(savedInstanceState)

        supportActionBar?.hide()

        authViewModel = ViewModelProvider(this).get(AuthViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        binding.lifecycleOwner = this
        binding.authViewModel = authViewModel

        setContentView(binding.root)

        initGoogleSignInClient()
        setClickListeners()
    }

    /**
     * Init Sign in via Google.
     */
    private fun initGoogleSignInClient() {
        val googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)
    }

    /**
     * Sets click listeners to buttons.
     */
    private fun setClickListeners() {
        binding.googleSignInButton.setOnClickListener { signIn() }
        binding.btnLogin.setOnClickListener { loginWithEmail() }
        binding.btnRegister.setOnClickListener { goToRegisterActivity() }
    }

    /**
     * Launches Google sign in screen.
     */
    private fun signIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    /**
     * Validates data and makes a login attempt.
     */
    private fun loginWithEmail() {
        if (binding.txtEmailAddress.text.isNullOrEmpty() || binding.txtPassword.text.isNullOrEmpty()) {
            Toast.makeText(this, "Please fill in both values.", Toast.LENGTH_SHORT).show()
            return
        }
        authViewModel.signInWithEmailAndPassword()
        Toast.makeText(this, "Signing in...", Toast.LENGTH_SHORT).show()
        authViewModel.authenticatedUserLiveData?.observe(this) { user ->
            if (user.isAuthenticated) {
                goToMainActivity()
            } else {
                Toast.makeText(this, "Auhentication not successful", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val googleSignInAccount = task.getResult(ApiException::class.java)
                googleSignInAccount?.let { getGoogleAuthCredential(it) }
            } catch (e: ApiException) {
                Log.e("Google_auth", e.message.toString())
            }
        }
    }

    /**
     * Gets credential from Google auth servers.
     */
    private fun getGoogleAuthCredential(googleSignInAccount: GoogleSignInAccount) {
        val googleTokenId = googleSignInAccount.idToken
        val googleAuthCredential = GoogleAuthProvider.getCredential(googleTokenId, null)
        signInWithGoogleAuthCredential(googleAuthCredential)
    }

    /**
     * Signs in with Google.
     */
    private fun signInWithGoogleAuthCredential(googleAuthCredential: AuthCredential) {
        authViewModel.signInWithGoogle(googleAuthCredential)
        authViewModel.authenticatedUserLiveData?.observe(this) { authenticatedUser ->
            if (authenticatedUser.isNew) {
                createNewUser(authenticatedUser)
                goToSetUserDetailsActivity()
            } else {
                goToMainActivity()
            }
        }
    }

    /**
     * Creates new user in DB.
     */
    private fun createNewUser(authenticatedUser: User) {
        authViewModel.createUser(authenticatedUser)
        authViewModel.createdUserLiveData?.observe(this) { user ->
            if (user.isCreated) {
                toastMessage(user.name.orEmpty())
            }
        }
    }

    private fun toastMessage(name: String) {
        Toast.makeText(this, "Hi $name!\nYour account was successfully created.", Toast.LENGTH_LONG).show()
    }

    private fun goToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun goToRegisterActivity() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun goToSetUserDetailsActivity() {
        val intent = Intent(this, SetUserDetailsActivity::class.java)
        startActivity(intent)
        finish()
    }
}