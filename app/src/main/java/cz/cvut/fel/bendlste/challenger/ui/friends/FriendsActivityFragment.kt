package cz.cvut.fel.bendlste.challenger.ui.friends

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.SearchView.OnQueryTextListener
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.databinding.FriendsActivityFragmentBinding
import cz.cvut.fel.bendlste.challenger.model.FriendsActivity
import cz.cvut.fel.bendlste.challenger.ui.explore.ChallengeDetailActivity
import cz.cvut.fel.bendlste.challenger.viewModel.friends.FriendsActivityViewModel

const val QUERY_HINT = "Search for users"

class FriendsActivityFragment: Fragment(R.layout.friends_activity_fragment) {
    private var _binding: FriendsActivityFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var friendsActivityViewModel: FriendsActivityViewModel
    private lateinit var adapter: FriendsActivityAdapter
    private lateinit var friendsActivityList: List<FriendsActivity>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FriendsActivityFragmentBinding.bind(view)
        friendsActivityViewModel = ViewModelProvider(this.requireActivity()).get(FriendsActivityViewModel::class.java)

        setUpUI()
        fetchData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Sets up [FriendsActivityAdapter] and friends searchbar.
     */
    private fun setUpUI() {
        adapter = FriendsActivityAdapter(
            { challengeId -> adapterOnClick(challengeId) },
            {friendsActivityId -> onLike(friendsActivityId)},
            {friendsActivityId -> onDislike(friendsActivityId)},
            requireContext())
        val recyclerView: RecyclerView = binding.faRecycler

        recyclerView.adapter = adapter

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager

        val dividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            layoutManager.orientation
        )
        recyclerView.addItemDecoration(dividerItemDecoration)

        val searchView = binding.searchView
        searchView.queryHint = QUERY_HINT


        searchView.isSubmitButtonEnabled = true
        searchView.isIconifiedByDefault = false

        searchView.setOnQueryTextListener(object : OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                val name = p0.orEmpty()
                friendsActivityViewModel.getUserByName(name).observe(viewLifecycleOwner, {
                    if (it != null) {
                        onSearchSuccess(it.uuid)
                    } else {
                        onSearchFailure()
                    }
                })
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                return false
            }
        })
    }

    /**
     * Retrieves data from Firebase Database and passes them to adapter.
     */
    private fun fetchData() {
        friendsActivityViewModel.friendIds.observe(viewLifecycleOwner, {friendsIds ->
            friendsActivityViewModel.fetchFriendsActivity(friendsIds.orEmpty()).observe(viewLifecycleOwner, { list ->
                friendsActivityList = list
                if (list.isNotEmpty()) {
                    adapter.submitList(friendsActivityList)
                }
            })
        })
    }

    /**
     * Launches [ChallengeDetailActivity].
     */
    private fun adapterOnClick(challengeId: String) {
        val intent = Intent(this.requireActivity(), ChallengeDetailActivity::class.java)
        intent.putExtra("CHALLENGE_ID", challengeId)
        startActivity(intent)
    }

    /**
     * Adds a like to friends activity.
     */
    private fun onLike(friendsActivityId: String) = friendsActivityViewModel.like(friendsActivityId)

    /**
     * Adds a dislike to friends activity.
     */
    private fun onDislike(friendsActivityId: String) = friendsActivityViewModel.dislike(friendsActivityId)

    /**
     * Handles search for user when successful.
     */
    private fun onSearchSuccess(userId: String) {
        val intent = Intent(this.requireContext(), UserProfileActivity::class.java)
        intent.putExtra("USER_ID", userId)
        startActivity(intent)
    }

    /**
     * Handles search for user when not successful.
     */
    private fun onSearchFailure() {
        Toast.makeText(this.requireContext(), "User with such name does not exist.", Toast.LENGTH_LONG).show()
    }
}
