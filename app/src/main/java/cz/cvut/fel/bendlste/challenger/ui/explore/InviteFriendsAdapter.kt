package cz.cvut.fel.bendlste.challenger.ui.explore

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.User
import cz.cvut.fel.bendlste.challenger.utils.GlideApp

class InviteFriendsAdapter(
    private val onClick: (String) -> Unit,
    private val inviteOnClick: (String) -> Boolean,
    private val context: Context
) : ListAdapter<User, InviteFriendsAdapter.ViewHolder>(UserDiffCallback) {

    class ViewHolder(
        view: View,
        private val onClick: (String) -> Unit,
        private val inviteOnClick: (String) -> Boolean,
        private val context: Context
    ) : RecyclerView.ViewHolder(view) {
        private val imageView: ImageView = view.findViewById(R.id.ia_image)
        private val name: TextView = view.findViewById(R.id.ia_name)
        private val btnInvite: ImageView = view.findViewById(R.id.ia_invite)
        private lateinit var currentItem: User

        init {
            // Define click listener for the ViewHolder's View.
            itemView.setOnClickListener {
                onClick(currentItem.uuid)
            }

            btnInvite.setOnClickListener {
                if (inviteOnClick(currentItem.uuid)) {
                    btnInvite.setImageResource(R.drawable.ic_done)
                }
            }
        }

        /* Bind attributes. */
        fun bind(user: User) {
            currentItem = user

            Firebase.storage.reference.child("profile_pictures").child(user.uuid).metadata.addOnSuccessListener {
                GlideApp
                    .with(context)
                    .load(Firebase.storage.reference.child("profile_pictures").child(user.uuid))
                    .circleCrop()
                    .into(imageView)
            }.addOnFailureListener {
                imageView.setImageResource(R.drawable.default_avatar)
            }

            name.text = currentItem.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.invite_adapter_item, parent, false)
        return ViewHolder(view, onClick, inviteOnClick, context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = getItem(position)
        holder.bind(user)
    }
}