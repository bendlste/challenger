package cz.cvut.fel.bendlste.challenger.viewModel.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.Badge
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FriendsViewModel @Inject constructor(private val usersRepository: UsersRepository) : ViewModel() {

    /**
     * Friend ids of current users.
     */
    val friendsIdsLiveData = usersRepository.getFriendsIds()

    /**
     * Badges of current user.
     */
    val badges: LiveData<List<Badge>>
        get() = usersRepository.getBadgesOfCurrentUser()

    /**
     * Retrieves User objects from friend [ids].
     */
    fun getFriendsFromIds(ids: List<String>) = usersRepository.getUsersFromIds(ids)
}
