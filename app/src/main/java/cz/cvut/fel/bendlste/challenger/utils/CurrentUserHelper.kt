package cz.cvut.fel.bendlste.challenger.utils

import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

object CurrentUserHelper {
    val currentUserId: String
        get() = Firebase.auth.currentUser?.uid ?: ""

    val currentUserName: String
        get() = Firebase.auth.currentUser?.displayName ?: ""
}