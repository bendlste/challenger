package cz.cvut.fel.bendlste.challenger.ui.register

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.databinding.ActivityRegisterBinding
import cz.cvut.fel.bendlste.challenger.viewModel.register.RegisterViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterActivity: AppCompatActivity() {

    private lateinit var registerViewModel: RegisterViewModel

    private lateinit var binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.hide()
        registerViewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        binding.lifecycleOwner = this
        binding.registerViewModel = registerViewModel

        setContentView(binding.root)

        binding.btnRegister.setOnClickListener { register() }
    }

    /**
     * Validates inputs and registers user.
     */
    private fun register() {
        if (!validateInputs()) {
            return
        }

        registerViewModel.registerUserWithEmail()
        val intent = Intent(this, SetUserDetailsActivity::class.java)
        startActivity(intent)
        finish()
    }

    /**
     * Validates inputs.
     */
    private fun validateInputs(): Boolean {
        if (binding.txtEmailAddress.text.isNullOrEmpty() ||
            binding.txtPassword.text.isNullOrEmpty() || binding.txtPasswordVerify.text.isNullOrEmpty()
        ) {
            Toast.makeText(this, "Please enter data into every field.", Toast.LENGTH_SHORT).show()
            return false
        } else if (binding.txtPassword.text.toString() != binding.txtPasswordVerify.text.toString()) {
            Toast.makeText(this, "Passwords does not match.", Toast.LENGTH_SHORT).show()
            return false
        } else if (!binding.txtEmailAddress.text.isValidEmail()) {
            Toast.makeText(this, "Email not valid.", Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }
}

/**
 * Validates email.
 */
fun CharSequence?.isValidEmail() = !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()
