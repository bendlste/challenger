package cz.cvut.fel.bendlste.challenger.viewModel.explore

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.repository.ChallengeRepository
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class InviteFriendsViewModel @Inject constructor(
    private val challengeRepository: ChallengeRepository,
    private val usersRepository: UsersRepository
) : ViewModel() {

    /**
     * Friend ids of current user.
     */
    val friendIds: LiveData<List<String>>
        get() = usersRepository.getFriendsIds()

    /**
     * Retrieves Challenge object from [challengeId].
     */
    fun getChallenge(challengeId: String) = challengeRepository.getChallengeById(challengeId)

    /**
     * Add [challenge] invite for user with id [invitedUserId].
     */
    fun addChallengeInvite(challenge: Challenge, invitedUserId: String) = challengeRepository.addChallengeInvite(challenge, invitedUserId)

    /**
     * Retrieves User objects from [friendIds].
     */
    fun getFriendsFromIds(friendIds: List<String>) = usersRepository.getUsersFromIds(friendIds)
}