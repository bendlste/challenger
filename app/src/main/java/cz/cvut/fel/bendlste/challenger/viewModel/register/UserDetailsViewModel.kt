package cz.cvut.fel.bendlste.challenger.viewModel.register

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class UserDetailsViewModel @Inject constructor(
    private val authRepository: AuthRepository
): ViewModel() {

    val fullName: MutableLiveData<String> = MutableLiveData()
    val age: MutableLiveData<String> = MutableLiveData()

    /**
     * Updates user details.
     */
    fun setUserDetails(profilePicture: Bitmap?) {
        val ageValue = age.value?.toInt() ?: 0

        authRepository.setUserDetails(fullName.value.orEmpty(), ageValue, profilePicture)
    }
}