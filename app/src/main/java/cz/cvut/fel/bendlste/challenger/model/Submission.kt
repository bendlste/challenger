package cz.cvut.fel.bendlste.challenger.model

import com.google.firebase.database.Exclude
import java.io.Serializable

/**
 * Represents submission of a user to a challenge.
 */
data class Submission(
    val challengeId: String,
    val userId: String,
    val value: Int,
    val timestamp: Long
): Serializable {

    companion object {
        /**
         * Converts HashMap with proper values to [Submission].
         */
        @Exclude
        fun convertFromHashMap(hashMap: HashMap<String, Any>): Submission {
            val challengeId = hashMap["challengeId"] as String
            val userId = hashMap["userId"] as String
            val value = (hashMap["value"] as Long).toInt()
            val timestamp = hashMap["timestamp"] as Long

            return Submission(challengeId, userId, value, timestamp)
        }
    }
}
