package cz.cvut.fel.bendlste.challenger.model

import android.graphics.Bitmap
import com.google.firebase.database.Exclude
import java.util.UUID

/**
 * Represents friends activity posts.
 */
data class FriendsActivity(
    val uuid: String = UUID.randomUUID().toString(),
    val challengeId: String,
    val challengeName: String,
    val userId: String,
    val userName: String,
    val addedValue: Int,
    val sumValues: Int,
    val m1: Int,
    val m2: Int,
    var upVotes: HashMap<String, String>,
    var downVotes: HashMap<String, String>,
    val comment: String
) {

    @get:Exclude
    var image: Bitmap? = null

    var isWinnerAnnouncment = false

    /**
     * Checks if first milestone is reached.
     */
    fun isM1Reached(): Boolean {
        return sumValues >= m1
    }

    /**
     * Checks if second milestone is reached.
     */
    fun isM2Reached(): Boolean {
        return sumValues >= m2
    }

    companion object {
        /**
         * Converts HashMap with proper values to [FriendsActivity].
         */
        @Exclude
        fun convertFromHashMap(hashMap: HashMap<String, String>): FriendsActivity {
            val uuid = hashMap["uuid"] ?: ""
            val challengeId = hashMap["challengeId"] ?: ""
            val challengeName = hashMap["challengeName"] ?: ""
            val userId = hashMap["userId"] ?: ""
            val userName = hashMap["userName"] ?: ""
            val addedValue = if (hashMap.containsKey("addedValue")) {
                (hashMap["addedValue"] as Long?)?.toInt() ?: 0
            } else 0
            val sumValues = (hashMap["sumValues"] as Long?)?.toInt() ?: 0
            val m1 = (hashMap["m1"] as Long?)?.toInt() ?: 0
            val m2 = (hashMap["m2"] as Long?)?.toInt() ?: 0
            val upVotes = if (hashMap.containsKey("upVotes")) {
                (hashMap["upVotes"] as HashMap<String, String>?) ?: HashMap()
            } else HashMap()
            val downVotes = if (hashMap.containsKey("downVotes")) {
                (hashMap["downVotes"] as HashMap<String, String>?) ?: HashMap()
            } else HashMap()

            val comment = if (hashMap.containsKey("comment")) {
                hashMap["comment"] ?: ""
            } else ""

            val winnerAnnouncment = if (hashMap.containsKey("winnernnouncment")) {
                hashMap["winnerAnnouncment"].toBoolean()
            } else false

            return FriendsActivity(
                uuid,
                challengeId,
                challengeName,
                userId,
                userName,
                addedValue,
                sumValues,
                m1,
                m2,
                upVotes,
                downVotes,
                comment
            ).apply {
                isWinnerAnnouncment = winnerAnnouncment
            }
        }
    }
}
