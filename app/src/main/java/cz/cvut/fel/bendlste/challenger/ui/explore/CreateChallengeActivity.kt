package cz.cvut.fel.bendlste.challenger.ui.explore

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.text.InputType
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import cz.cvut.fel.bendlste.challenger.MainActivity
import cz.cvut.fel.bendlste.challenger.databinding.ActivityCreateChallengeBinding
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.ChallengeType
import cz.cvut.fel.bendlste.challenger.utils.GlideApp
import cz.cvut.fel.bendlste.challenger.viewModel.explore.CreateChallengeViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.io.FileNotFoundException
import java.io.IOException
import java.util.Calendar
import java.util.UUID

private const val IMAGE_REQUEST_CODE = 9961

@AndroidEntryPoint
class CreateChallengeActivity : AppCompatActivity() {
    private lateinit var createChallengeViewModel: CreateChallengeViewModel

    private lateinit var binding: ActivityCreateChallengeBinding

    private lateinit var image: Bitmap
    private var from: Long = 0
    private var to: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createChallengeViewModel = ViewModelProvider(this).get(CreateChallengeViewModel::class.java)
        binding = ActivityCreateChallengeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpUI()
        setOnClickListeners()
    }

    /**
     * Sets up UI.
     */
    private fun setUpUI() {
        supportActionBar?.hide()
        val spinnerItems = ChallengeType.values()
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        binding.spinner.adapter = adapter

        binding.ccFrom.inputType = InputType.TYPE_NULL
        binding.ccTo.inputType = InputType.TYPE_NULL
    }

    /**
     * Used for getting image from gallery.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val selectedImage = data?.data
            try {
                image = MediaStore.Images.Media.getBitmap(applicationContext.contentResolver, selectedImage)
                GlideApp.with(applicationContext)
                    .load(selectedImage)
                    .fitCenter()
                    .into(binding.ccImage)

                binding.ccBtnSelect.visibility = View.GONE
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Sets click listeners for buttons and calendar fields.
     */
    private fun setOnClickListeners() {
        val viewFrom = binding.ccFrom
        val viewTo = binding.ccTo

        // Launch date picker dialog for "From" field.
        binding.ccFrom.setOnClickListener { fromView ->
            val cldr = Calendar.getInstance()
            val day = cldr[Calendar.DAY_OF_MONTH]
            val month = cldr[Calendar.MONTH]
            val year = cldr[Calendar.YEAR]
            // date picker dialog
            val picker = DatePickerDialog(
                this,
                { view, year, monthOfYear, dayOfMonth ->
                    from =
                        Calendar.getInstance().apply {
                            set(Calendar.YEAR, year)
                            set(Calendar.MONTH, monthOfYear)
                            set(Calendar.DAY_OF_MONTH, dayOfMonth)
                        }.timeInMillis
                    viewFrom.setText("$dayOfMonth/${monthOfYear + 1}/$year")
                }, year, month, day
            )
            picker.show()
        }

        // Launch date picker dialog for "to" field.
        binding.ccTo.setOnClickListener {
            val cldr = Calendar.getInstance()
            val day = cldr[Calendar.DAY_OF_MONTH]
            val month = cldr[Calendar.MONTH]
            val year = cldr[Calendar.YEAR]
            // date picker dialog
            val picker = DatePickerDialog(
                this,
                { view, year, monthOfYear, dayOfMonth ->
                    to =
                        Calendar.getInstance().apply {
                            set(Calendar.YEAR, year)
                            set(Calendar.MONTH, monthOfYear)
                            set(Calendar.DAY_OF_MONTH, dayOfMonth)
                        }.timeInMillis

                    viewTo.setText("$dayOfMonth/${monthOfYear + 1}/$year")
                }, year, month, day
            )
            picker.show()
        }

        // Converts all binding values to strings / ints and creates a challenge.
        binding.btnCreate.setOnClickListener {
            val name = binding.ccName.text.toString()
            val description = binding.ccDescription.text.toString()
            val bet = binding.ccBet.text.toString()
            val selected = binding.spinner.selectedItem.toString()
            val targetValue = binding.ccTarget.text.toString().toInt()
            val m1 = binding.ccM1.text.toString().toInt()
            val m2 = binding.ccM2.text.toString().toInt()
            val milestones = listOf(m1, m2)

            val challenge = Challenge(
                UUID.randomUUID().toString(),
                name,
                description,
                bet,
                from,
                to,
                enumValueOf(selected),
                targetValue,
                milestones
            )
            challenge.picture = image
            createChallengeViewModel.addChallenge(challenge)
            createChallengeViewModel.joinChallenge(challenge.uuid)
            goToMainActivity()
        }

        binding.ccBtnSelect.setOnClickListener {
            getImageFromGallery()
        }
    }

    /**
     * Opens gallery and lets user select a picture.
     */
    private fun getImageFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_REQUEST_CODE)
    }

    /**
     * Launches Main Activity.
     */
    private fun goToMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}