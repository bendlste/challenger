package cz.cvut.fel.bendlste.challenger.model.repository

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.model.FriendsActivity
import cz.cvut.fel.bendlste.challenger.utils.CurrentUserHelper.currentUserId
import java.io.ByteArrayOutputStream

const val TAG_FRIENDS_ACTIVITY = "FRIENDS_ACTIVITY"

class FriendsActivityRepository {
    // Database
    private val ref = Firebase.database.reference.child("friends_activity")

    // Storage
    private val friendsActivityImagesRef = Firebase.storage.reference.child("friends_activity")

    /**
     * Creates a friend activity object in the database.
     *
     * @param friendsActivity friends activity to be added
     */
    fun createFriendsActivity(friendsActivity: FriendsActivity) {
        ref.child(friendsActivity.uuid).setValue(friendsActivity).addOnSuccessListener {
            Log.i(TAG_FRIENDS_ACTIVITY, "Added FriendsActivity: $friendsActivity to the database.")
        }.addOnFailureListener {
            Log.e(TAG_FRIENDS_ACTIVITY, "Adding of FriendsActivity $friendsActivity to the database failed.")
        }
    }

    /**
     * Submits friends activity picture to the Firebase Storage.
     *
     * @param friendsActivity friends activity with the picture to be stored.
     */
    fun submitFriendsActivityPicture(friendsActivity: FriendsActivity) {
        friendsActivity.image?.let {
            // Add image.
            val baos = ByteArrayOutputStream()
            val bitmap = it
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data = baos.toByteArray()

            friendsActivityImagesRef.child(friendsActivity.uuid).putBytes(data).addOnSuccessListener {
                Log.i(TAG_FRIENDS_ACTIVITY, "Added picture for friends activity ${friendsActivity.uuid}.")
            }.addOnFailureListener{
                Log.e(TAG_FRIENDS_ACTIVITY, "Adding picture for friends activity ${friendsActivity.uuid} FAILED.")
            }
        }
    }

    /**
     * Retrieves friends activites of users based on their ids.
     *
     * @param friendsIds ids of users whose friends activities you want to retrieve
     * @return LiveData object which would be filled with list of suitable friends activities
     */
    fun getFriendsActivityForUser(friendsIds: List<String>): LiveData<List<FriendsActivity>> {
        val toRet = MutableLiveData<List<FriendsActivity>>()
        val list = mutableListOf<FriendsActivity>()

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (item in snapshot.children) {
                    val hashMap = item.value as HashMap<String, String>
                    if (hashMap["userId"] in friendsIds) {
                        val activity = FriendsActivity.convertFromHashMap(hashMap)
                        list.add(activity)
                    }
                }
                toRet.value = list
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_FRIENDS_ACTIVITY, "Reading friends activity for user failed.")
            }
        }

        for (id in friendsIds) {
            ref.orderByChild("userId").equalTo(id).addValueEventListener(listener)
        }

        return toRet
    }

    /**
     * Adds a like to friends activity.
     *
     * @param friendsActivityId friends activity id to like
     */
    fun like(friendsActivityId: String) {
        ref.child(friendsActivityId).child("upVotes").child(currentUserId).setValue(currentUserId)
    }

    /**
     * Adds a dislike to friends activity.
     *
     * @param friendsActivityId friends activity id to dislike
     */
    fun dislike(friendsActivityId: String) {
        ref.child(friendsActivityId).child("downVotes").child(currentUserId).setValue(currentUserId)
    }
}