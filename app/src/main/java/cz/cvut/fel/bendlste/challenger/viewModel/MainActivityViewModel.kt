package cz.cvut.fel.bendlste.challenger.viewModel

import androidx.lifecycle.ViewModel
import cz.cvut.fel.bendlste.challenger.model.FriendsActivity
import cz.cvut.fel.bendlste.challenger.model.repository.ChallengeRepository
import cz.cvut.fel.bendlste.challenger.model.repository.FriendsActivityRepository
import cz.cvut.fel.bendlste.challenger.model.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    private val usersRepository: UsersRepository,
    private val challengeRepository: ChallengeRepository,
    private val friendsActivityRepository: FriendsActivityRepository
) : ViewModel() {

    /**
     * Challenge invites of current user.
     */
    val challengeInvites = challengeRepository.getChallengeInvitesForCurrentUser()

    /**
     * Friends additions of current user.
     */
    val friendsAdditions = usersRepository.getFriendsAdditionForCurrentUser()

    /**
     * Retrieves all not evaluated challenges.
     */
    fun checkForDoneChallenges() = challengeRepository.getAllNotEvaluated()

    /**
     * Gets all submissions for challenge with id [challengeId].
     */
    fun getAllSubmissionsForChallenge(challengeId: String) = challengeRepository.getAllSubmissionsForChallenge(challengeId)

    /**
     * Marks challenge with id [challengeId] as evaluated.
     */
    fun markChallengeAsEvaluated(challengeId: String) = challengeRepository.setAsEvaluated(challengeId)

    /**
     * Retrieves User object based on [userId].
     */
    fun getUserById(userId: String) = usersRepository.getUserById(userId)

    /**
     * Adds [friendsActivity] entry to DB.
     */
    fun createFriendsActivity(friendsActivity: FriendsActivity) = friendsActivityRepository.createFriendsActivity(friendsActivity)

    /**
     * Removes all challenge invites for current user.
     */
    fun removeChallengeInvites() = challengeRepository.removeAllChallengeInvitesForCurrentUser()
}