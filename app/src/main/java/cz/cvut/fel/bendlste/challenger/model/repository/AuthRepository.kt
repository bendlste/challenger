package cz.cvut.fel.bendlste.challenger.model.repository

import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.model.User
import java.io.ByteArrayOutputStream


const val TAG_AUTH = "FirebaseAuth"

class AuthRepository {
    private val auth = Firebase.auth
    private val database = Firebase.database.reference
    private val usersRef = database.child("users")

    /**
     * Signs in with Google.
     *
     * @param googleAuthCredential credential from Google auth server
     * @return live data which will be filled with user object.
     */
    fun firebaseSignInWithGoogle(googleAuthCredential: AuthCredential?): LiveData<User> {
        val authenticatedUserMutableLiveData: MutableLiveData<User> = MutableLiveData()
        if (googleAuthCredential != null) {
            auth.signInWithCredential(googleAuthCredential).addOnCompleteListener { authTask ->
                if (authTask.isSuccessful) {
                    val isNewUser: Boolean = authTask.result?.additionalUserInfo!!.isNewUser
                    val firebaseUser: FirebaseUser? = auth.currentUser
                    if (firebaseUser != null) {
                        val uid = firebaseUser.uid
                        val name = firebaseUser.displayName
                        val email = firebaseUser.email
                        val user = User(uid, name.orEmpty(), email.orEmpty())
                        user.isNew = isNewUser
                        authenticatedUserMutableLiveData.value = user
                    }
                } else {
                    Log.e(TAG_AUTH, authTask.exception?.message.toString())
                }
            }
        }
        return authenticatedUserMutableLiveData
    }

    /**
     * Creates user in Firebase Realtime Database if there is no such user.
     *
     * @param authenticatedUser
     */
    fun createUserInDatabaseIfNotExists(authenticatedUser: User): LiveData<User> {
        val newUserMutableLiveData = MutableLiveData<User>()
        val uuid = authenticatedUser.uuid
        val uidRef = usersRef.child(uuid)
        uidRef.get().addOnCompleteListener { uidTask ->
            if (uidTask.isSuccessful) {
                val result = uidTask.result
                if (result == null || !result.exists()) {
                    usersRef.push().key
                    usersRef.child(uuid).setValue(authenticatedUser).addOnCompleteListener {
                        if (it.isSuccessful) {
                            authenticatedUser.isCreated = true
                            newUserMutableLiveData.value = authenticatedUser
                        } else {
                            Log.e(TAG_AUTH, it.exception?.message.toString())
                        }
                    }
                } else {
                    newUserMutableLiveData.setValue(authenticatedUser)
                }
            } else {
                Log.e(TAG_AUTH, uidTask.exception?.message.toString())
            }
        }
        return newUserMutableLiveData
    }

    /**
     * Registers user with email and password.
     *
     * @param email email address of user
     * @param password password of user
     */
    fun registerWithEmailAndPassword(email: String?, password: String?) {
        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            Log.d(TAG_AUTH, "Provided values are not correct")
            return
        }
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d(TAG_AUTH, "createUserWithEmail:success")
                    val firebaseUser = task.result?.user
                    requireNotNull(firebaseUser)
                    val uuid = firebaseUser.uid
                    val user = User(uuid = uuid, email = email, name = null)
                    user.isAuthenticated = true
                    createUserInDatabaseIfNotExists(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG_AUTH, "createUserWithEmail:failure", task.exception)
                }
            }
    }

    /**
     * Signs up user with email and password.
     *
     * @param email email address of user
     * @param password users password
     *
     * @return LiveData object which would be filled with authenticated [User] object.
     */
    fun signUpWithEmailAndPassword(email: String, password: String): LiveData<User> {
        val authenticatedUserMutableLiveData: MutableLiveData<User> = MutableLiveData()
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG_AUTH, "signInWithEmail:success")
                    val firebaseUser: FirebaseUser? = auth.currentUser
                    if (firebaseUser != null) {
                        val uid = firebaseUser.uid
                        val name = firebaseUser.displayName
                        val email = firebaseUser.email
                        val user = User(uid, name.orEmpty(), email.orEmpty())
                        user.isAuthenticated = true
                        authenticatedUserMutableLiveData.value = user
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG_AUTH, "signInWithEmail:failure", task.exception)
                    }
                } else {
                    val user = User("", "", "", -1)
                    user.isAuthenticated = false
                    authenticatedUserMutableLiveData.value = user
                }
            }
        return authenticatedUserMutableLiveData
    }

    /**
     * Sets details of user in the registration process.
     */
    fun setUserDetails(fullName: String, age: Int, profilePicture: Bitmap?) {
        val currentUser = auth.currentUser!!
        updateInAuth(fullName, currentUser)
        updateInDB(fullName, age, currentUser)
        profilePicture?.let {
            updateProfilePicture(profilePicture, currentUser.uid)
        }
    }

    /**
     * Logs out current user from application.
     */
    fun logOut() {
        auth.signOut()
    }

    /**
     * Updates users attributes in Firebase Authentication.
     */
    private fun updateInAuth(fullName: String, currentUser: FirebaseUser) {
        val profileUpdates = UserProfileChangeRequest.Builder()
            .setDisplayName(fullName)
            .build()
        currentUser.updateProfile(profileUpdates)
    }

    /**
     * Updates users attributes in Firebase Realtime Database.
     */
    private fun updateInDB(fullName: String, age: Int, currentUser: FirebaseUser) {
        val key = currentUser.uid
        val userRef = usersRef.child(key)
        userRef.child("fullName").setValue(fullName)
        userRef.child("age").setValue(age)
    }

    /**
     * Updates users profile picture in Firebase Storage.
     */
    private fun updateProfilePicture(profilePicture: Bitmap, uuid: String) {
        val ref = Firebase.storage.reference.child("profile_pictures").child(uuid)
        val baos = ByteArrayOutputStream()
        profilePicture.compress(Bitmap.CompressFormat.JPEG, 70, baos)
        val data = baos.toByteArray()

        val uploadTask = ref.putBytes(data)
        uploadTask.addOnFailureListener {
            Log.d("Firestorage", "Uploading image failed")
        }.addOnSuccessListener {
            Log.d("Firestorage", "Uploading image successful")
        }
    }
}