package cz.cvut.fel.bendlste.challenger

import android.app.Application
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        // Keeps app usable even offline.
        Firebase.database.setPersistenceEnabled(true)
        Firebase.database.reference.keepSynced(true)

        super.onCreate()
    }
}