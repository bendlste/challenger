package cz.cvut.fel.bendlste.challenger.ui.friends

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.databinding.ActivityUserProfileBinding
import cz.cvut.fel.bendlste.challenger.ui.explore.ChallengeDetailActivity
import cz.cvut.fel.bendlste.challenger.ui.profile.BadgesAdapter
import cz.cvut.fel.bendlste.challenger.utils.GlideApp
import cz.cvut.fel.bendlste.challenger.viewModel.friends.UserProfileViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserProfileActivity : AppCompatActivity() {

    private lateinit var userId: String
    private lateinit var binding: ActivityUserProfileBinding
    private lateinit var userProfileViewModel: UserProfileViewModel
    private lateinit var adapter: BadgesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserProfileBinding.inflate(layoutInflater)
        supportActionBar?.hide()

        userProfileViewModel = ViewModelProvider(this).get(UserProfileViewModel::class.java)
        setContentView(binding.root)

        getUserIdFromIntent()
        setUpAdapter()
        fetchData()
        setOnClickListeners()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    /**
     * Retrieves [userId] from intent.
     */
    private fun getUserIdFromIntent() {
        val value = intent.getSerializableExtra("USER_ID")
        userId = if (value is String) {
            value
        } else {
            ""
        }
    }

    /**
     * Sets up [BadgesAdapter].
     */
    private fun setUpAdapter() {
        adapter = BadgesAdapter({ challenge -> badgesAdapterOnClick(challenge) }, this)
        val badgesRecyclerView = binding.badgesRecyclerView
        badgesRecyclerView.adapter = adapter

        val layoutManagerBadges = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        badgesRecyclerView.layoutManager = layoutManagerBadges
    }

    /**
     * Retrieves data from Firebase Database.
     */
    private fun fetchData() {
        userProfileViewModel.friendIds.observe(this, {
            if (userId in it) {
                hideAddFriendButton()
            }
        })

        val storageRef = Firebase.storage.reference.child("profile_pictures").child(userId)

        storageRef.metadata.addOnSuccessListener {
            GlideApp
                .with(this)
                .load(storageRef)
                .circleCrop()
                .into(binding.imgProfilePicture)
        }.addOnFailureListener {
            binding.imgProfilePicture.setImageResource(R.drawable.default_avatar)
        }

        userProfileViewModel.getUser(userId).observe(this, {
            if (it != null) {
                binding.txtName.text = it.name
            }
        })

        userProfileViewModel.getBadges(userId).observe(this, {
            if (it != null) {
                adapter.submitList(it)
            }
        })
    }

    /**
     * Sets click listeners for butons.
     */
    private fun setOnClickListeners() {
        binding.btnAddFriend.setOnClickListener {
            userProfileViewModel.addFriend(userId)
            hideAddFriendButton()
            Toast.makeText(this, "Successfully added to friends", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Hides "Add as friend" button.
     */
    private fun hideAddFriendButton() {
        binding.btnAddFriend.visibility = View.GONE
    }

    /**
     * Opens [ChallengeDetailActivity].
     */
    private fun badgesAdapterOnClick(challengeId: String) {
        val intent = Intent(this, ChallengeDetailActivity::class.java)
        intent.putExtra("CHALLENGE_ID", challengeId)
        startActivity(intent)
    }
}