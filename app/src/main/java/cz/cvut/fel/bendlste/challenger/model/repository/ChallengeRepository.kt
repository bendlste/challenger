package cz.cvut.fel.bendlste.challenger.model.repository

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.Submission
import cz.cvut.fel.bendlste.challenger.utils.CurrentUserHelper
import java.io.ByteArrayOutputStream

const val TAG_CHALLENGE = "CHALLENGE_DB"
const val TAG_SUBMISSION = "SUBMISSION_DB"

class ChallengeRepository {
    // Database
    private val challengeRef = Firebase.database.reference.child("challenges")
    private val submissionsRef = Firebase.database.reference.child("submissions")
    private val challengeInvitesRef = Firebase.database.reference.child("challenge_invites")

    // Storage
    private val challengeImagesRef = Firebase.storage.reference.child("challenges")

    /**
     * Retrieves [Challenge] based on its [id].
     *
     * @param id id of the challenge to retrieve
     * @return LiveData object which would be filled with the [Challenge] object.
     */
    fun getChallengeById(id: String): LiveData<Challenge?> {
        val challenge = MutableLiveData<Challenge?>()

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val result = snapshot.value as HashMap<String, Any?>
                    val parsedChallenge = Challenge.convertFromHashMap(result)
                    challenge.value = parsedChallenge
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_CHALLENGE, error.message)
            }
        }

        challengeRef.child(id).addValueEventListener(listener)

        return challenge
    }

    /**
     * Creates [challenge] in database and adds a picture to storage.
     *
     * @param challenge challenge to create
     */
    fun addChallenge(challenge: Challenge) {
        // Create in database.
        challengeRef.child(challenge.uuid).setValue(challenge)

        // Add image.
        val baos = ByteArrayOutputStream()
        val bitmap = challenge.picture
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        challengeImagesRef.child(challenge.uuid).putBytes(data).addOnSuccessListener {
            Log.d(TAG_CHALLENGE, "Picture added to challenge with id: ${challenge.uuid}")
        }.addOnFailureListener {
            Log.e(TAG_CHALLENGE, "Failed to add picture to challenge with id: ${challenge.uuid}")
        }
    }

    /**
     * Retrieves all challenges from database.
     *
     * @return LiveData object which would be filled with list of challenges
     */
    fun getAllChallenges(): LiveData<List<Challenge>> {
        val toRet = MutableLiveData<List<Challenge>>()

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val challenges = mutableListOf<Challenge>()
                val values = snapshot.value as HashMap<String, Any?>
                for (value in values) {
                    val challenge = Challenge.convertFromHashMap(value.value as HashMap<String, Any?>)
                    challenges.add(challenge)
                }
                toRet.value = challenges
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_CHALLENGE, error.message)
            }
        }

        challengeRef.addValueEventListener(listener)
        return toRet
    }

    /**
     * Adds a [submission] to the database.
     *
     * @param submission submission to add
     */
    fun addSubmission(submission: Submission) {
        val challengeId = submission.challengeId
        val userId = submission.userId
        val refToPush = submissionsRef.child(challengeId).child(userId)
        val key = refToPush.push().key
        key?.let {
            refToPush.child(key).setValue(submission)
        }
    }

    /**
     * Retrieves all submission of user with id [userId] in a challenge with id [challengeId].
     *
     * @param challengeId challengeId of the challenge to get submissions from
     * @param userId userId of the user to get submissions form
     *
     * @return LiveData object which would be filled with list of suitable submissons
     */
    fun getSubmissionsOfUser(challengeId: String, userId: String): LiveData<List<Submission>> {
        val toRet = MutableLiveData<List<Submission>>()
        val submissions = mutableListOf<Submission>()
        val ref = submissionsRef.child(challengeId).child(userId)

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (item in snapshot.children) {
                    val result = item.value as HashMap<String, Any>
                    val submission = Submission.convertFromHashMap(result)
                    submissions.add(submission)
                }
                toRet.value = submissions
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_SUBMISSION, error.message)
            }
        }

        ref.addValueEventListener(listener)

        return toRet
    }

    /**
     * Retrieves all submissions for a challenge with id [challengeId].
     *
     * @param challengeId challengeId of the challenge to retrieve submissions from
     *
     * @return LiveData object which would be filled with list of suitable submissions
     */
    fun getAllSubmissionsForChallenge(challengeId: String): LiveData<List<Submission>> {
        val toRet = MutableLiveData<List<Submission>>()
        val submissions = mutableListOf<Submission>()
        val ref = submissionsRef.child(challengeId)

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (user in snapshot.children) {
                    for (item in user.children) {
                        val result = item.value as HashMap<String, Any>
                        val submission = Submission.convertFromHashMap(result)
                        submissions.add(submission)
                    }
                }
                toRet.value = submissions
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_SUBMISSION, error.message)
            }
        }

        ref.addValueEventListener(listener)

        return toRet
    }


    /**
     * Retrieves [Challenge] objects from challenge [ids].
     *
     * @param ids ids of the challenges to retrieve
     *
     * @return LiveData object which would be filled with list of suitable challenges
     */
    fun getChallengesFromIds(ids: List<String>): LiveData<List<Challenge>> {
        val toRet = MutableLiveData<List<Challenge>>()
        val challenges = mutableListOf<Challenge>()

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (item in snapshot.children) {
                    if (item.key in ids) {
                        val result = item.value as HashMap<String, Any?>
                        val parsedChallenge = Challenge.convertFromHashMap(result)
                        challenges.add(parsedChallenge)
                    }
                }
                toRet.value = challenges
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_CHALLENGE, error.message)
            }
        }

        challengeRef.addValueEventListener(listener)

        return toRet
    }

    /**
     * Adds current user as a participant to challenge with id [challengeId].
     *
     * @param challengeId id of the challenge to add a participant to
     */
    fun addParticipant(challengeId: String) {
        val userId = Firebase.auth.currentUser?.uid ?: return
        val refToPush = challengeRef.child(challengeId).child("participants").child(userId)
        refToPush.setValue(userId)
    }

    /**
     * Retrieves all not yet evaluated challenges.
     *
     * @return LiveData object which would be filled with list of suitable challenges
     */
    fun getAllNotEvaluated(): LiveData<List<Challenge>> {
        val toRet = MutableLiveData<List<Challenge>>()
        val task = challengeRef.orderByChild("isEvaluated").equalTo(false).get()

        task.addOnSuccessListener { snapshot ->
            val challenges = mutableListOf<Challenge>()
            for (item in snapshot.children) {
                val hashMap = item.value as HashMap<String, Any?>
                challenges.add(Challenge.convertFromHashMap(hashMap))
            }
            toRet.value = challenges
        }
        return toRet
    }

    /**
     * Sets a challenge as evaluated.
     *
     * @param challengeId challenge id of the challenge to be set as evaluated
     */
    fun setAsEvaluated(challengeId: String) {
        challengeRef.child(challengeId).child("isEvaluated").setValue(true)
    }

    /**
     * Adds a challenge invite to database.
     *
     * @param challenge challenge to invite to
     * @param invitedUserId userId of the user to be invited
     */
    fun addChallengeInvite(challenge: Challenge, invitedUserId: String) {
        challengeInvitesRef.child(invitedUserId).child(challenge.uuid).setValue(challenge.name)
    }

    /**
     * Retrieves challenge invites for current user.
     *
     * @return LiveData object which would be filled with HashMap<[Challenge.uuid],[Challenge.name]> of suitable invites
     */
    fun getChallengeInvitesForCurrentUser(): LiveData<HashMap<String, String>> {
        val toRet = MutableLiveData<HashMap<String, String>>()

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.value?.let {
                    toRet.value = it as HashMap<String, String>
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_CHALLENGE, error.message)
            }
        }

        challengeInvitesRef.child(CurrentUserHelper.currentUserId).addValueEventListener(listener)
        return toRet
    }

    /**
     * Removes all challenge invites for current user.
     */
    fun removeAllChallengeInvitesForCurrentUser() {
        challengeInvitesRef.child(CurrentUserHelper.currentUserId).removeValue()
    }
}