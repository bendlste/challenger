package cz.cvut.fel.bendlste.challenger.model.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.model.Badge
import cz.cvut.fel.bendlste.challenger.model.User
import cz.cvut.fel.bendlste.challenger.utils.CurrentUserHelper
import java.io.StringBufferInputStream

const val TAG_USER = "USER_DB"

class UsersRepository(val context: Context) {
    // Database
    private val database = Firebase.database.reference
    private val usersRef = database.child("users")
    private val friendAdditionRef = database.child("friend_addition")

    // Auth
    private val currentUser: FirebaseUser?
        get() = Firebase.auth.currentUser

    /**
     * Retrieves current user as a [User] object.
     *
     * @return LiveData object which would be filled with [User] object
     */
    fun getCurrentUserAsUser(): LiveData<User?>? {
        val cUser = currentUser
        return if (cUser != null) {
            getUserById(cUser.uid)
        } else {
            null
        }
    }

    /**
     * Retrieves [User] object based on its id.
     *
     * @param uuid id of the user to retrieve
     *
     * @return LiveData object which would be filled with proper [User] object
     */
    fun getUserById(uuid: String): LiveData<User?> {
        val ref = usersRef.child(uuid)
        val userData = MutableLiveData<User?>()

        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val hashMap = snapshot.value as HashMap<String, Any?>
                    userData.value = User.convertFromHashMap(hashMap)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e("Database", error.message)
            }
        }
        ref.addValueEventListener(valueEventListener)

        return userData
    }

    /**
     * Adds a friend to current user.
     *
     * @param uuid id the of the user to add as a friend
     */
    fun addFriend(uuid: String) {
        currentUser?.let {
            val currentUserUuid = it.uid
            val friendsRef = usersRef.child(currentUserUuid).child("friends")
            friendsRef.child(uuid).push()
            friendsRef.child(uuid).setValue(uuid)
        }
    }

    /**
     * Adds a friend addition entry, which should be converted to friends addition notification.
     *
     * @param userId id of the user which were added to [currentUser]s friends list
     */
    fun addFriendAdditionEntry(userId: String) {
        currentUser?.let {
            val currentUserUuid = it.uid
            val currentUserName = it.displayName
            friendAdditionRef.child(userId).child(currentUserUuid).setValue(currentUserName)
        }
    }

    /**
     * Removes friend addition entry from user with id [userId].
     */
    fun removeFriendAdditionEntry(userId: String) {
        currentUser?.let {
            val currentUserId = it.uid
            friendAdditionRef.child(currentUserId).child(userId).removeValue()
        }
    }

    /**
     * Retrieves all friend addition entries for current user.
     *
     * @return LiveData object which would be filled with HashMap<[User.uuid], [User.name]>
     */
    fun getFriendsAdditionForCurrentUser(): LiveData<List<String>> {
        val toRet = MutableLiveData<List<String>>()
        val names = mutableListOf<String>()

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.value?.let {
                    val hashMap = it as HashMap<String, String>
                    for (key in hashMap.keys) {
                        removeFriendAdditionEntry(key)
                    }
                    names.addAll(hashMap.values)

                    toRet.value = names
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_USER, error.message)
            }
        }

        currentUser?.let {
            val currentUserUuid = it.uid
            friendAdditionRef.child(currentUserUuid).addValueEventListener(listener)
        }

        return toRet
    }

    /**
     * Retrieves [User] objects based on their ids.
     *
     * @param ids ids of users to retrieve
     *
     * @return LiveData object which would be filled with list of suitable Users
     */
    fun getUsersFromIds(ids: List<String>): LiveData<List<User>> {
        val toRet = MutableLiveData<List<User>>()
        val users = mutableListOf<User>()

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (item in snapshot.children) {
                    if (item.key in ids) {
                        val result = item.value as HashMap<String, Any?>
                        val parsedUser = User.convertFromHashMap(result)
                        users.add(parsedUser)
                    }
                }
                toRet.value = users
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_CHALLENGE, error.message)
            }
        }

        usersRef.addValueEventListener(listener)

        return toRet
    }

    /**
     * Retrieves friend ids of current user.
     *
     * @return LiveData object which would be filled with list of friend ids
     */
    fun getFriendsIds(): LiveData<List<String>> {
        val toRet = MutableLiveData<List<String>>()
        val ids = mutableListOf<String>()

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (item in snapshot.children) {
                    ids.add(item.value.toString())
                }
                toRet.value = ids
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_USER, error.message)
            }
        }
        currentUser?.let {
            usersRef.child(it.uid).child("friends").addValueEventListener(listener)
        }

        return toRet
    }

    /**
     * Adds a challenge to current user database object.
     *
     * @param challengeId challenge id to add
     */
    fun addChallenge(challengeId: String) {
        currentUser?.let {
            val currentUserUuid = it.uid
            val challengeRef = usersRef.child(currentUserUuid).child("challenges")
            challengeRef.child(challengeId).setValue(challengeId)
        }
    }

    /**
     * Retrieves all challenge ids of current user.
     *
     * @return LiveData object which would be filled with list of challenge ids
     */
    fun getChallengeIdsOfCurrentUser(): LiveData<List<String>> {
        val toRet = MutableLiveData<List<String>>()
        val challenges: MutableList<String> = mutableListOf()

        currentUser?.let {
            val currentUserUuid = it.uid
            val challengesRef = usersRef.child(currentUserUuid).child("challenges")
            val listener = object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.value != null) {
                        val challengeIds = snapshot.value as HashMap<String, String>
                        for (item in challengeIds.values) {
                            challenges.add(item)
                        }
                    }
                    toRet.value = challenges
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.e(TAG_USER, error.message)
                }
            }
            challengesRef.addValueEventListener(listener)
        }
        return toRet
    }

    /**
     * Retrieves [User] objects from [ids].
     *
     * @param ids ids to retrieve
     *
     * @return LiveData object which would be filled with list of suitable [User] objects
     */
    fun getUsersByIds(ids: List<String>): LiveData<List<User>> {
        val users = MutableLiveData<List<User>>()

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val suitableUsers = mutableListOf<User>()
                for (userSnapshot in snapshot.children) {
                    val result = userSnapshot.value as HashMap<String, Any?>
                    if (result["uuid"] in ids) {
                        suitableUsers.add(User.convertFromHashMap(result))
                    }
                }
                users.value = suitableUsers
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_USER, error.message)
            }
        }

        usersRef.addValueEventListener(listener)

        return users
    }

    /**
     * Retrieves user with matching "fullName" attribute.
     *
     * @param name name to match
     *
     * @return LiveData object which would be filled with suitable [User] object
     */
    fun getUserByName(name: String): LiveData<User?> {
        val toRet = MutableLiveData<User?>()

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (item in snapshot.children) {
                        val hashMap = item.value as HashMap<String, Any?>
                        toRet.value = User.convertFromHashMap(hashMap)
                    }
                } else {
                    toRet.value = null
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_USER, error.message)
            }
        }

        usersRef.orderByChild("fullName").equalTo(name).addValueEventListener(listener)

        return toRet
    }

    /**
     * Adds [badge] to current user.
     *
     * @param badge badge to add to current user.
     */
    fun addBadge(badge: Badge) {
        currentUser?.uid?.let { uid ->
            usersRef.child(uid).child("badges").child(badge.challengeId).setValue(badge.challengeName)
        }
    }

    /**
     * Retrieves badges of current user.
     *
     * @return LiveData with list of badges
     */
    fun getBadgesOfCurrentUser(): LiveData<List<Badge>> {
        return getBadgesOfUser(CurrentUserHelper.currentUserId)
    }

    fun getBadgesOfUser(userId: String): LiveData<List<Badge>> {
        val toRet = MutableLiveData<List<Badge>>()

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val hashMap = snapshot.value as HashMap<String, String>
                    toRet.value = Badge.convertFromHashMap(hashMap)
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG_USER, error.message)
            }
        }

        usersRef.child(userId).child("badges").addValueEventListener(listener)

        return toRet
    }
}
