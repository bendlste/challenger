package cz.cvut.fel.bendlste.challenger.ui.register

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.core.graphics.drawable.toBitmap
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import cz.cvut.fel.bendlste.challenger.MainActivity
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.databinding.ActivityEnterUserDetailsBinding
import cz.cvut.fel.bendlste.challenger.utils.GlideApp
import cz.cvut.fel.bendlste.challenger.viewModel.register.UserDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.io.FileNotFoundException
import java.io.IOException

const val REQUEST_CODE = 333

@AndroidEntryPoint
class SetUserDetailsActivity: AppCompatActivity() {

    private lateinit var binding: ActivityEnterUserDetailsBinding

    private lateinit var userDetailsViewModel: UserDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.hide()
        userDetailsViewModel = ViewModelProvider(this).get(UserDetailsViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_enter_user_details)
        binding.lifecycleOwner = this
        binding.userDetailsViewModel = userDetailsViewModel

        setContentView(binding.root)

        setClickListeners()
    }

    /**
     * Set click listeners for buttons.
     */
    private fun setClickListeners() {
        binding.btnGallery.setOnClickListener { getImageFromGallery() }
        binding.btnRegister.setOnClickListener { finishRegister() }
    }

    /**
     * Opens gallery and lets user select a picture.
     */
    private fun getImageFromGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val selectedImage = data?.data
            try {
                GlideApp.with(this)
                    .load(selectedImage)
                    .circleCrop()
                    .into(binding.imgProfile)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Sends all the data to database.
     */
    private fun finishRegister() {
        val profilePicture = binding.imgProfile.drawable?.toBitmap(640, 640)

        if (binding.txtFullName.text.isNullOrEmpty() ||
            binding.txtAge.text.isNullOrEmpty()
        ) {
            Toast.makeText(this, "Please enter data into every field.", Toast.LENGTH_SHORT).show()
            return
        }

        userDetailsViewModel.setUserDetails(profilePicture)

        this.getSharedPreferences("AppInfo", MODE_PRIVATE)
            .edit{ putBoolean("OnboardingDone", true) }

        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}