package cz.cvut.fel.bendlste.challenger.ui.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.model.User
import cz.cvut.fel.bendlste.challenger.utils.GlideApp

class FriendsAdapter(private val onClick: (User) -> Unit, private val context: Context) :
    ListAdapter<User, FriendsAdapter.ViewHolder>(UserDiffCallback) {

    class ViewHolder(view: View, val onClick: (User) -> Unit, private val context: Context) : RecyclerView.ViewHolder(view) {
        private val imageView: ImageView = view.findViewById(R.id.img_item)
        private val textView: TextView = view.findViewById(R.id.txt_item_name)
        private var currentUser: User? = null

        init {
            // Define click listener for the ViewHolder's View.
            itemView.setOnClickListener {
                currentUser?.let {
                    onClick(it)
                }
            }
        }

        /* Bind user name and profile picture. */
        fun bind(user: User) {
            currentUser = user

            textView.text = currentUser?.name.orEmpty()

            Firebase.storage.reference.child("profile_pictures").child(user.uuid).metadata.addOnSuccessListener {
                GlideApp
                    .with(context)
                    .load(Firebase.storage.reference.child("profile_pictures").child(user.uuid))
                    .circleCrop()
                    .into(imageView)
            }.addOnFailureListener {
                imageView.setImageResource(R.drawable.default_avatar)
            }
        }
    }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_item, parent, false)
            return ViewHolder(view, onClick, context)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val user = getItem(position)
            holder.bind(user)
        }
    }

    object UserDiffCallback : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.uuid == newItem.uuid
        }
    }