package cz.cvut.fel.bendlste.challenger.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cz.cvut.fel.bendlste.challenger.R
import cz.cvut.fel.bendlste.challenger.databinding.DashboardFragmentBinding
import cz.cvut.fel.bendlste.challenger.model.Challenge
import cz.cvut.fel.bendlste.challenger.model.Submission
import cz.cvut.fel.bendlste.challenger.ui.explore.ChallengeDetailActivity
import cz.cvut.fel.bendlste.challenger.viewModel.dashboard.DashboardViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment : Fragment(R.layout.dashboard_fragment) {

    private var _binding: DashboardFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var dashboardViewModel: DashboardViewModel
    private lateinit var activeChallenges: List<Challenge>
    private val submissions = HashMap<String, List<Submission>>()
    private lateinit var activeChallengesAdapter: ActiveChallengesAdapter
    private lateinit var upcomingChallengesAdapter: UpcomingChallengesAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = DashboardFragmentBinding.bind(view)
        dashboardViewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)

        setUpUI()
        fetchData()
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    /**
     * Retrieves data from Firebase Database and passes them to adapters.
     */
    private fun fetchData() {
        dashboardViewModel.challengeIds.observe(viewLifecycleOwner, { ids ->

            // Challenges START
            dashboardViewModel.getChallengesFromIds(ids).observe(viewLifecycleOwner, { challenges ->
                // Filter only active
                activeChallenges = challenges.filter { it.isActive() }

                // Submit to adapters
                activeChallengesAdapter.submitList(activeChallenges)
                upcomingChallengesAdapter.submitList(challenges.filter { it.isUpcoming() }.distinct())
            })

            for (id in ids) {
                // Submissions START
                dashboardViewModel.getSubmissionsForCurrentUser(id).observe(viewLifecycleOwner, {
                    // Save in the fragment instance
                    submissions[id] = it
                    // Re-setUp the adapter, so that the submissions are correct
                    setUpActiveChallengesAdapter()
                    activeChallengesAdapter.submitList(activeChallenges)
                })
                // Submissions END
            }
            // Challenges END
        })
    }

    private fun setUpUI() {
        setUpActiveChallengesAdapter()
        setUpUpcomingChallengesAdapter()
    }

    /**
     * Launches [ChallengeDetailActivity].
     */
    private fun adapterOnClick(challenge: Challenge) {
        val intent = Intent(context, ChallengeDetailActivity::class.java)
        intent.putExtra("CHALLENGE_ID", challenge.uuid)
        startActivity(intent)
    }

    /**
     * Sets up [ActiveChallengesAdapter]
     */
    private fun setUpActiveChallengesAdapter() {
        activeChallengesAdapter = ActiveChallengesAdapter(
            { challenge -> adapterOnClick(challenge) },
            submissions,
            requireContext()
        )
        val activeChallengesRecyclerView: RecyclerView = binding.activeChallengesRecycler

        activeChallengesRecyclerView.adapter = activeChallengesAdapter

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        activeChallengesRecyclerView.layoutManager = layoutManager

        val dividerItemDecoration = DividerItemDecoration(
            activeChallengesRecyclerView.context,
            layoutManager.orientation
        )
        activeChallengesRecyclerView.addItemDecoration(dividerItemDecoration)
    }

    /**
     * Sets up [UpcomingChallengesAdapter]
     */
    private fun setUpUpcomingChallengesAdapter() {
        upcomingChallengesAdapter = UpcomingChallengesAdapter(
            { challenge -> adapterOnClick(challenge) },
            requireContext()
        )
        val upcomingChallengesRecyclerView: RecyclerView = binding.upcomingChallengesRecycler

        upcomingChallengesRecyclerView.adapter = upcomingChallengesAdapter

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        upcomingChallengesRecyclerView.layoutManager = layoutManager

        val dividerItemDecoration = DividerItemDecoration(
            upcomingChallengesRecyclerView.context,
            layoutManager.orientation
        )
        upcomingChallengesRecyclerView.addItemDecoration(dividerItemDecoration)

    }
}